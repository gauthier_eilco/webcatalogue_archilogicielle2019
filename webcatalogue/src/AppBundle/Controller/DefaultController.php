<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Model\Produit;
use AppBundle\Model\Vente;
use Doctrine\ORM\EntityManager;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
       

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/api/produit", name="add-produit")
     * @Method({"POST"})
     */
    public function AddProduitAction(Request $request)
    {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');

        $data = json_decode($request->getContent(), true);
        $produit = new Produit($data);


        //Ajouter le prix (table prix)
        $prix_id = $service->addPrice($produit);
        $produit->setPrixId($prix_id);

        //Ajouter le stock (table stock)
        $stock_id = $service->addStock($produit);
        $produit->setStockId($stock_id);

        //Ajouter le produit_vente (table produit_vente)
        /*$produit_vente_id = $service->addProduitVente($produit);
        $produit->setProduitVenteId($produit_vente_id);
*/
        //Ajouter le produit
        return new JsonResponse($service->addProduit($produit));

    }

    /**
     * @Route("/api/categories", name="get-categories")
     * @Method({"GET"})
     */
    public function getCategoriesAction() {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        return new JsonResponse($service->getCategories());
    }

    /**
     * @Route("/api/produits", name="get-produits")
     * @Method({"GET"})
     */
    public function getProduitsAction() {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        return new JsonResponse($service->getProduits());
    }


    /**
     * @Route("/api/produit/{id}", name="update-produit")
     * @Method({"PUT"})
     */
    public function updateProduitAction(Request $request, $id) {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        
        $data = json_decode($request->getContent(), true);
        $produit = new Produit($data);


        //Update prix du produit
        $service->updatePrix($produit);

        //update stock du produit
        $service->updateStock($produit);

        //update produit
        $service->updateProduit($produit, intval($id));

        return new JsonResponse();
       

    }


    /**
     * @Route("/api/produit/{id}", name="remove-produit")
     * @Method({"POST"})
     */
    public function deleteProduitAction(Request $request, $id) {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        
        $data = json_decode($request->getContent(), true);
        $produit = new Produit($data);

        //vérifier si aucune vente n'est associée au produit - si aucune on peut le supprimer
        $cantBeDeleted = $service->isAlreadyInVente($produit->getId());

        if(!$cantBeDeleted) {
             //supprimer le produit
            $service->deleteProduit(intval($id));
            return new JsonResponse();
        }

        //mettre sa disponibilité à 0 
        $service->deleteProduitDispo(intval($id));
        return new JsonResponse();

       /*

        //supprimer prix du produit
        $service->deletePrix($produit);

        //supprimer stock du produit
        $service->deleteStock($produit);

        //SUPPRIMER VENTE SI IL Y A 

        $service->deleteVenteOfProduit($produit);

        //supprimer produit_vente
        $service->deleteProduitVente($produit);
*/
        
       

    }


    /**
     * @Route("/api/vente", name="create-vente")
     * @Method({"POST"})
     */
    public function createVenteAction(Request $request) {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        
        $data = json_decode($request->getContent(), true);
        $vente = new Vente($data['vente']);
        $produits = $data['produits'];

        


        //insertion table vente

        $id_vente = $service->insertVente($vente);
       
        //Insertions table produit_vente
        for($i =0; $i < sizeof($produits); $i++) {
            $produits[$i] = new Produit($produits[$i]);
            $service->insertProduitVente($produits[$i], $id_vente);

            //update des stocks des produits
            $service->updateStock($produits[$i]);
        } 

        
        return new JsonResponse();
       

    }

    /**
     * @Route("/api/ventes", name="get-ventes")
     * @Method({"GET"})
     */
    public function getVentesAction() {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        return new JsonResponse($service->getVentes());
        
    }

     /**
     * @Route("/api/vente/{id}", name="get-vente")
     * @Method({"GET"})
     */
    public function getVenteAction($id) {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        return new JsonResponse($service->getVente($id));
        
    }

    /**
     * @Route("api/ventes/produit/{id}", name="get-ventes-produit")
     * @Method({"GET"})
     */
    public function getVentesActionProduit($id) {
        // On récupère le service
        $service = $this->container->get('webcatalogueService');
        return new JsonResponse($service->getVentesOfProduit($id));
        
    }
    



    
    
}
