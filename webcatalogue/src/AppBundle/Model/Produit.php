<?php

namespace AppBundle\Model;

class Produit {

	//Attributs d'un produit
	private $id = null;
	private $nom =  null;
	private $description =  null;
	private $categorie_id =  null;
	private $categorie_libelle =  null;
	private $stock_id =  null;
	private $stock_dernier_maj = null;
	private $stock_quantite = null;
	private $prix_id = null;
	private $prix_valeur = null;
	private $prix_date_debut = null;
	private $prix_date_fin = null;
	private $produit_vente_id = null;
	private $quantite_vendue = null;
	private $disponible = null;



		public function __construct($array = null) {
			if(!empty($array)) $this->hydrate($array);
		}


		public function hydrate($array) {
			if(isset($array['id'])) $this->setId($array['id']);
			if(isset($array['nom'])) $this->setNom($array['nom']);
			if(isset($array['description'])) $this->setDescription($array['description']);
			if(isset($array['categorie_id'])) $this->setCategorieId($array['categorie_id']);
			if(isset($array['categorie_libelle'])) $this->setCategorieLibelle($array['categorie_libelle']);
			if(isset($array['stock_id'])) $this->setStockId($array['stock_id']);
			if(isset($array['stock_dernier_maj'])) $this->setStockDernierMaj($array['stock_dernier_maj']);
			if(isset($array['stock_quantite'])) $this->setStockQuantite($array['stock_quantite']);
			if(isset($array['prix_id'])) $this->setPrixId($array['prix_id']);
			if(isset($array['prix_valeur'])) $this->setPrixValeur($array['prix_valeur']);
			if(isset($array['prix_date_debut_formated'])) $this->setPrixDateDebut($array['prix_date_debut_formated']);
			if(isset($array['prix_date_fin_formated'])) $this->setPrixDateFin($array['prix_date_fin_formated']);
			if(isset($array['produit_vente_id'])) $this->setProduitVenteId($array['produit_vente_id']);
			if(isset($array['quantite_vendue'])) $this->setQuantiteVendue($array['quantite_vendue']);
			if(isset($array['disponible'])) $this->setDisponible($array['disponible']);
		}


		//Getters
		public function getId() {
			return $this->id;
		}

		public function getNom() {
			return $this->nom;
		}

		public function getDescription() {
			return $this->description;
		}

		public function getCategorieId() {
			return $this->categorie_id;
		}

		public function getCategorieLibelle() {
			return $this->categorie_libelle;
		}

		public function getStockId() {
			return $this->stock_id;
		}

		public function getStockDernierMaj() {
			return $this->stock_dernier_maj;
		}

		public function getStockQuantite() {
			return $this->stock_quantite;
		}

		public function getPrixId() {
			return $this->prix_id;
		}

		public function getPrixValeur() {
			return $this->prix_valeur;
		}

		public function getPrixDateDebut() {
			return $this->prix_date_debut;
		}

		public function getPrixDateFin() {
			return $this->prix_date_fin;
		}

		public function getProduitVenteId() {
			return $this->produit_vente_id;
		}

		public function getQuantiteVendue() {
			return $this->quantite_vendue;
		}


		//Setters
		public function setId($id) {
			$this->id = $id;
		}

		public function setNom($nom) {
			$this->nom = $nom;
		}

		public function setDescription($description) {
			$this->description = $description;
		}

		public function setCategorieId($categorie_id) {
			$this->categorie_id = $categorie_id;
		}

		public function setCategorieLibelle($categorie_libelle) {
			$this->categorie_libelle = $categorie_libelle;
		}

		public function setStockId($stock_id) {
			$this->stock_id = $stock_id;
		}

		public function setStockDernierMaj($stock_dernier_maj) {
			$this->stock_dernier_maj = $stock_dernier_maj;
		}

		public function setStockQuantite($stock_quantite) {
			$this->stock_quantite = intval($stock_quantite);
		}

		public function setPrixId($prix_id) {
			$this->prix_id = $prix_id;
		}

		public function setPrixValeur($prix_valeur) {
			if(!is_float($prix_valeur))
				$prix_valeur = floatval($prix_valeur . '.00');
			$this->prix_valeur = $prix_valeur;
		}

		public function setPrixDateDebut($prix_date_debut) {
			$this->prix_date_debut = $prix_date_debut;
		}

		public function setPrixDateFin($prix_date_fin) {
			$this->prix_date_fin = $prix_date_fin;
		}

		public function setProduitVenteId($produit_vente_id) {
			$this->produit_vente_id = $produit_vente_id;
		}

		public function setQuantiteVendue($quantite_vendue) {
			$this->quantite_vendue = intval($quantite_vendue);
		}

		public function setDisponible($dispo) {
			$this->disponible = $dispo;
		}


}

?>