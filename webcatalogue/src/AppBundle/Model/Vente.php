<?php

namespace AppBundle\Model;

class Vente {

	//Attributs d'un produit
	private $id_vente = null;
	private $date_vente =null;
	private $prix_totale = null;
	private $valider = null;
	private $nb_articles = null;


		public function __construct($array = null) {
			if(!empty($array)) $this->hydrate($array);
		}


		public function hydrate($array) {
			if(isset($array['id'])) $this->setId($array['id']);
			if(isset($array['date'])) $this->setDateVente($array['date']);
			if(isset($array['prix_total'])) $this->setPrixTotal($array['prix_total']);
			if(isset($array['valider'])) $this->setValider($array['valider']);
			if(isset($array['total_produits'])) $this->setNbArticles($array['total_produits']);
		}


		//Getters
		public function getId() {
			return $this->id_vente;
		}

		public function getDateVente() {
			return $this->date_vente;
		}

		public function getPrixTotal() {
			return $this->prix_totale;
		}

		public function getValider() {
			return $this->valider;
		}

		public function getNbArticles() {
			return $this->nb_articles;
		}

		//Setters
		public function setId($id) {
			$this->id_vente = $id;
		}

		public function setDateVente($date) {
			$this->date_vente = $date;
		}

		public function setPrixTotal($prix) {
			$this->prix_totale = $prix;
		}

		public function setValider($valider) {
			$this->valider = $valider;
		}

		public function setNbArticles($nb) {
			$this->nb_articles = $nb;
		}

}

?>