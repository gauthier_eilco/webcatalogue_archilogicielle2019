<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

class webcatalogueService {

	/**
     *
     * @var Connection 
     */
    protected $conn;


    public function __construct(EntityManager $em) {
    	$this->conn = $em->getConnection();
    }

    public function getCategories() {
    	$qb = $this->conn->createQueryBuilder();
    	$qb ->select('c.id as value, c.libelle as libelle')
    		->from('webcatalogue.categorie', 'c')
    		->orderBy('c.libelle');

    	return $qb->execute()->fetchAll();
		
	}

	public function addPrice($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->insert('webcatalogue.prix')
			->values(
				array(
					'valeur' => ':valeur',
					'date_debut' => ':start',
					'date_fin' => ':end'
				)
			)
			->setParameter('valeur', $produit->getPrixValeur())
			->setParameter('start', $produit->getPrixDateDebut())
			->setParameter('end', $produit->getPrixDateFin());
		
		$qb->execute();
		return $this->conn->lastInsertId();
	}

	public function addStock($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->insert('webcatalogue.stock_produit')
			->values(
				array(
					'dernier_maj' => ':maj',
					'Quantite' => ':quantite'
				)
			)
			->setParameter('maj', date('Y-m-d H:i:s'))
			->setParameter('quantite', $produit->getStockQuantite());
		
		$qb->execute();
		return $this->conn->lastInsertId();
	}

	public function addProduitVente($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->insert('webcatalogue.produit_vente')
			->setValue('quantite_vendue', '?')
			->setParameter(0, $produit->getQuantiteVendue());
		
		$qb->execute();
		return $this->conn->lastInsertId();
	}

	public function addProduit($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->insert('webcatalogue.produit')
			->values(
				array(
					'nom' => ':nom',
					'description' => ':description',
					'categorie_id' => ':categorie_id',
					'stock_id' => ':stock_id',
					'prix_id' => ':prix_id',
					'disponible' => 1
				)
			)
			->setParameter('nom', $produit->getNom())
			->setParameter('description', $produit->getDescription())
			->setParameter('categorie_id', $produit->getCategorieId())
			->setParameter('stock_id', $produit->getStockId())
			->setParameter('prix_id', $produit->getPrixId());

		$qb->execute();
		return $this->conn->lastInsertId();
	}

	public function getProduits() {
		$qb = $this->conn->createQueryBuilder();
    	$qb ->select('p.id , p.nom, p.description, p.categorie_id, p.stock_id, p.prix_id')
    		->addSelect('pr.valeur as prix_valeur, pr.date_debut as prix_date_debut_formated, pr.date_fin as prix_date_fin_formated')
    		->addSelect('s.dernier_maj as stock_dernier_maj, s.Quantite as stock_quantite')
    		->addSelect('c.libelle as categorie_libelle')
    		->from('webcatalogue.produit', 'p')
    		->leftjoin('p', 'webcatalogue.prix', 'pr', 'p.prix_id = pr.id_prix')
    		->leftjoin('p', 'webcatalogue.stock_produit', 's', 'p.stock_id = s.id_stock')
    		->leftjoin('p', 'webcatalogue.categorie', 'c', 'p.categorie_id = c.id')
    		->where('p.disponible = :dispo')
    		->setParameter('dispo', 1)
    		->orderBy('p.nom')
    		->groupBy('p.id');

    	return $qb->execute()->fetchAll();
	}

	public function getVentesOfProduit($id) {
		$qb = $this->conn->createQueryBuilder();
    	$qb ->select('p.id_vente as id')
    		->from('webcatalogue.produit_vente', 'p')
    		->where('p.id_produit=:id')
    		->setParameter('id', $id);

    	return $qb->execute()->fetchAll();
	}


	public function updatePrix($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->update('webcatalogue.prix', 'p')
			->set('p.valeur', ':new_valeur')
			->set('p.date_debut', ':new_date_debut')
			->set('p.date_fin', ':new_date_fin')
			->where('p.id_prix = :prix_id')
			->setParameter('new_valeur', $produit->getPrixValeur())
			->setParameter('new_date_debut', $produit->getPrixDateDebut())
			->setParameter('new_date_fin', $produit->getPrixDateFin())
			->setParameter('prix_id', $produit->getPrixId());

		return $qb->execute();
	}

	public function updateStock($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->update('webcatalogue.stock_produit', 's')
			->set('s.dernier_maj', ':new_dernier_maj')
			->set('s.Quantite', ':new_quantite')
			->where('s.id_stock = :stock_id')
			->setParameter('new_dernier_maj', date('Y-m-d H:i:s'))
			->setParameter('new_quantite', $produit->getStockQuantite())
			->setParameter('stock_id', $produit->getStockId());

		return $qb->execute();
	}


	public function updateProduit($produit, $id) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->update('webcatalogue.produit', 'p')
			->set('p.nom', ':new_nom')
			->set('p.description', ':new_description')
			->set('p.categorie_id', ':new_categorie_id')
			->where('p.id = :id')
			->setParameter('new_nom', $produit->getNom())
			->setParameter('new_description', $produit->getDescription())
			->setParameter('new_categorie_id', $produit->getCategorieId())
			->setParameter('id', $id);

		return $qb->execute();
	} 


	public function isAlreadyInVente($id) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->select('p.*')
			->from('produit_vente', 'p')
			->where('p.id_produit = :id')
			->setParameter('id', $id);

		return sizeof($qb->execute()->fetchAll()) > 0;
	}

	public function deleteProduit($id) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->delete('webcatalogue.produit')
			->where('id = :id')
			->setParameter('id', $id);

		return $qb->execute();
	} 

	public function deleteProduitDispo($id) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->update('webcatalogue.produit', 'p')
			->set('p.disponible', ':dispo')
			->where('p.id = :id')
			->setParameter('dispo', 0)
			->setParameter('id', $id);

		return $qb->execute();
	}

	public function deletePrix($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->delete('webcatalogue.prix', 'p')
			->where('p.id_prix=:id')
			->setParameter('id', $produit->getPrixId());

		return $qb->execute();
	} 

	public function deleteStock($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->delete('webcatalogue.stock_produit', 's')
			->where('s.id_stock=:id')
			->setParameter('id', $produit->getStockId());

		return $qb->execute();
	} 

	public function deleteProduitVente($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->delete('webcatalogue.produit_vente', 'v')
			->where('v.id_produit_vente=:id')
			->setParameter('id', $produit->getProduitVenteId());

		return $qb->execute();
	} 

	public function deleteVenteOfProduit($produit) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->delete('webcatalogue.vente', 'v')
			->where('v.id_vente in (SELECT id_vente from produit_vente where id_produit = :id')
			->setParameter('id', $produit->getProduitVenteId());

		return $qb->execute();
	}

	public function insertVente($vente) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->insert('webcatalogue.vente')
			->values(
				array(
					'date_vente' => ':date',
					'prix_totale' => ':prix_totale',
					'valider' => ':valider'
				)
			)
			->setParameter('date', $vente->getDateVente())
			->setParameter('prix_totale', $vente->getPrixTotal())
			->setParameter('valider', 1);
		
		$qb->execute();
		return $this->conn->lastInsertId();
	}

	public function insertProduitVente($produit, $id_vente) {
		$qb = $this->conn->createQueryBuilder();
		$qb ->insert('webcatalogue.produit_vente')
			->values(
				array(
					'id_produit' => ':id_produit',
					'id_vente' => ':id_vente',
					'quantite_vendue' => ':quantite'
				)
			)
			->setParameter('id_produit', $produit->getId())
			->setParameter('id_vente', $id_vente)
			->setParameter('quantite',  $produit->getQuantiteVendue());
		
		$qb->execute();
		return true;
	}

	public function getVentes() {
		$qb = $this->conn->createQueryBuilder();
    	$qb ->select('v.id_vente as id , v.date_vente as date, v.prix_totale as prix_total')
    		->addSelect('count(pv.id_produit) as total_produits')
    		->from('webcatalogue.vente', 'v')
    		->innerJoin('v', 'produit_vente', 'pv', 'v.id_vente = pv.id_vente')
    		
    		->orderBy('v.date_vente', 'DESC')
    		->groupBy('v.id_vente');

    	return $qb->execute()->fetchAll();
	}

	public function getVente($id) {
		$qb = $this->conn->createQueryBuilder();
    	$qb 
    		->select('pv.quantite_vendue, p.nom, p.disponible, pp.valeur as prix_valeur, c.libelle as categorie_libelle' )
    		->from('webcatalogue.produit_vente', 'pv')
    		->innerJoin('pv', 'produit', 'p', 'pv.id_produit = p.id')
    		->innerJoin('p', 'prix', 'pp', 'p.prix_id = pp.id_prix')
    		->innerJoin('p', 'categorie', 'c', 'p.categorie_id = c.id')
    		->where('pv.id_vente = :id_vente')
    		->setParameter('id_vente', $id);

    		return $qb->execute()->fetchAll();
		
	}

}

?>