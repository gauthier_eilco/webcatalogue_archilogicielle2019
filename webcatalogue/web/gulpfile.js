var gulp = require('gulp');
var eslint = require('gulp-eslint');
var stylish = require('jshint-stylish');
var angularPlugin = require('eslint-plugin-angular');
var config =
 {
	 apps: ['public/webcatalogue/src/**/*.js']
 };


gulp.task('default', [], function () {
 return gulp.src(config.apps)
 .pipe(eslint(
         {"extends": ["eslint:recommended"]   
         }))
 .pipe(eslint.format())
});