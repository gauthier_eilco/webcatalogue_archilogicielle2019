module.exports = function (grunt) {

    var projectName = 'webcatalogue';

    grunt.initConfig({
        pkg: grunt.file.readJSON('../../package.json'),
        jasmine: {
            src: 'src/*.js',
            options: {
                specs: 'test/spec/*-spec.js',
                vendor: [
                    '../../web/vendor/angular/angular.min.js',
                    '../../web/vendor/angular-mocks/angular-mocks.js'
                ]
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: {
                files: {
                    './build/src/app.js': './src/webcatalogue.js',
                    './build/src/webcatalogue.service.js': './src/service/webcatalogue.service.js',
                    './build/src/webcatalogue.controller.js': './src/controller/webcatalogue.controller.js',
                    './build/src/webcatalogue.component.js': './src/components/webcatalogue.component.js',
                    './build/src/webcatalogue.model.js': './src/model/*.model.js'
                }
            }
        },
        concat: {
            js: {
                src: ['./build/src/app.js', './build/src/*.service.js', './build/src/*.controller.js', './build/src/*.component.js', './build/src/*.model.js'],
                dest: './build/' + projectName + '.js'
            }
        },
        uglify: {
            js: {
                src: ['./build/' + projectName + '.js'],
                dest: './build/' + projectName + '.min.js'
            }
        },
        copy: {
            main: {
                src: './build/' + projectName + '.min.js',
                dest: '../../web/public/' + projectName + '/' + projectName + '.min.js'
            }
        }
    });

    grunt.loadTasks('../../node_modules/grunt-contrib-concat/tasks');
    grunt.loadTasks('../../node_modules/grunt-contrib-uglify/tasks');
    grunt.loadTasks('../../node_modules/grunt-ng-annotate/tasks');
    grunt.loadTasks('../../node_modules/grunt-contrib-copy/tasks');
    grunt.loadTasks('../../node_modules/grunt-contrib-jasmine/tasks');

    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify', 'copy']); 
};