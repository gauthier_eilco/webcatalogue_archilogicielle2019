'use strict';

// Déclaration du module héritant du module orliModule
angular.module('webcatalogue', []);

'use strict';

function apiService($http, ValuesProvider) {
	return {
        sendProduitToBdd: function(produit) {
	    	return $http({
	    		method:"POST",
	    		url:ValuesProvider.getApiUrl() + "produit",
	    		data:produit
	    	});
	  	},
	  	getCategories: function() {
	  		return $http({
		      	method: "GET",
		        url: ValuesProvider.getApiUrl() + "categories"
	    	});
	  	},
	  	getProduits: function() {
	  		return $http({
		      	method: "GET",
		        url: ValuesProvider.getApiUrl() + "produits"
	    	});
	  	},
	  	updateProduit: function(produit) {
	  		return $http({
	  			method: "PUT",
	  			url: ValuesProvider.getApiUrl() + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	deleteProduit: function(produit) {
	  		return $http({
	  			method: "POST",
	  			url: ValuesProvider.getApiUrl() + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	createVente: function(panier) {
	  		return $http({
		  		method: 'POST',
		  		url: ValuesProvider.getApiUrl() + "vente",
		  		data: {vente : panier.vente, produits: panier.produits}
		  	});
	  	},
	  	getVentes: function() {
	  		return $http({
	  			method: 'GET',
	  			url: ValuesProvider.getApiUrl() + "ventes"
	  		});
	  	},
	  	getVentesOfProduit: function(produit) {
	  		return $http({
	  			method: 'GET',
	  			url: ValuesProvider.getApiUrl() + "ventes/produit/"+ produit.id
	  		});
	  	},
	  	getVente: function(id) {
	  		return $http({
	  			method: 'GET',
	  			url: ValuesProvider.getApiUrl() + "vente/"+id
	  		});
	  	}
    };
}

apiService.$inject = ['$http', 'ValuesProvider'];

angular
  .module('webcatalogue')
  .factory('APIService', apiService);




function appTools() {
	return {
		convertStrToDate: function(str) { //Convertir une date de l'input vers un format date object
          	var date;
          	if(str != "") date = new Date(str);
          	else date = new Date();

            var mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2),
            hr = ("0" + date.getHours()).slice(-2),
            mn = ("0" + date.getMinutes()).slice(-2),
            sc = ("0" + date.getSeconds()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-") + ' ' + hr+':'+mn +':'+sc;
        },
        formateDateForGrid: function(date) { //Formater la date en version française parce que vive la France
        	var formated = new Date(date),
            mnth = ("0" + (formated.getMonth() + 1)).slice(-2),
            day = ("0" + formated.getDate()).slice(-2);
        	return [
	        		day,
	        	 "-", 
	        			mnth,
	        	 "-", 
	        			formated.getFullYear(), 
	        	 " ", 
	        			formated.getHours(), 
	        	 "h", 
	        			formated.getMinutes()
        	].join('');
        },
        findProduitInPanier: function(panier, id_produit) {
        	var bool = false;
        	var prdt;
        	//Vérifier que le produit n'existe pas déjà dans le panier
	         panier.produits.forEach(function(p) {
	            if(p.id === id_produit) {
	              bool = true;
	              prdt = p;
	            }
	          });

	         return bool ? {find: true, produit: prdt} : {find: false, produit: null};
        }
	};
}


appTools.$inject = [];

angular
  .module('webcatalogue')
  .factory('AppTools', appTools);

function valuesProvider($window) {
	return {
		getModalObjectSuccessAjout : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalAjoutSuccess',
				title : 'Création d\'un nouveau produit',
				message : 'Votre produit a bien été ajouté'
			};
		},
		getModalObjectWarningPanier : function() {
			return {
				success : false,
				warning : true,
				id : 'printerModalListProduit',
				title : 'Supression du produit impossible',
				message : 'Le produit que vous souhaitez supprimer est actuellement dans le panier.'
			};
		},
		getModalObjectSuccessSuppressionProduit : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalListProduit',
				title : 'Supression du produit',
				message : 'Le produit a bien été supprimé.'
			};
		},
		getApiUrl : function() {
			return $window.location.protocol + '//' + $window.location.host + '/api/';
		}
	};
}

valuesProvider.$inject = ['$window'];

angular
  .module('webcatalogue')
  .factory('ValuesProvider', valuesProvider);

/*//service API qui sert à communiquer avec l'API (notre back symfony)
angular.module('webcatalogue').factory('APIService', ['$http', 'API_URL_CATALOGUE', function ($http, API_URL_CATALOGUE) {
    return {
        sendProduitToBdd: function(produit) {
	    	return $http({
	    		method:"POST",
	    		url:API_URL_CATALOGUE + "produit",
	    		data:produit
	    	});
	  	},
	  	getCategories: function() {
	  		return $http({
		      	method: "GET",
		        url: API_URL_CATALOGUE + "categories"
	    	});
	  	},
	  	getProduits: function() {
	  		return $http({
		      	method: "GET",
		        url: API_URL_CATALOGUE + "produits"
	    	});
	  	},
	  	updateProduit: function(produit) {
	  		return $http({
	  			method: "PUT",
	  			url: API_URL_CATALOGUE + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	deleteProduit: function(produit) {
	  		return $http({
	  			method: "POST",
	  			url: API_URL_CATALOGUE + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	createVente: function(panier) {
	  		return $http({
		  		method: 'POST',
		  		url: API_URL_CATALOGUE + "vente",
		  		data: {vente : panier.vente, produits: panier.produits}
		  	});
	  	},
	  	getVentes: function() {
	  		return $http({
	  			method: 'GET',
	  			url: API_URL_CATALOGUE + "ventes"
	  		});
	  	},
	  	getVentesOfProduit: function(produit) {
	  		return $http({
	  			method: 'GET',
	  			url: API_URL_CATALOGUE + "ventes/produit/"+ produit.id
	  		});
	  	},
	  	getVente: function(id) {
	  		return $http({
	  			method: 'GET',
	  			url: API_URL_CATALOGUE + "vente/"+id
	  		});
	  	}
    };
}]);


//service "outil" qui sert à tout et n'importe quoi
angular.module('webcatalogue').factory('AppTools', function() {
	return {
		convertStrToDate: function(str) { //Convertir une date de l'input vers un format date object
          	var date;
          	if(str != "") date = new Date(str);
          	else date = new Date();

            var mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2),
            hr = ("0" + date.getHours()).slice(-2),
            mn = ("0" + date.getMinutes()).slice(-2),
            sc = ("0" + date.getSeconds()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-") + ' ' + hr+':'+mn +':'+sc;
        },
        formateDateForGrid: function(date) { //Formater la date en version française parce que vive la France
        	var formated = new Date(date),
            mnth = ("0" + (formated.getMonth() + 1)).slice(-2),
            day = ("0" + formated.getDate()).slice(-2);
        	return [
	        		day,
	        	 "-", 
	        			mnth,
	        	 "-", 
	        			formated.getFullYear(), 
	        	 " ", 
	        			formated.getHours(), 
	        	 "h", 
	        			formated.getMinutes()
        	].join('');
        },
        findProduitInPanier: function(panier, id_produit) {
        	var bool = false;
        	var prdt;
        	//Vérifier que le produit n'existe pas déjà dans le panier
	         panier.produits.forEach(function(p) {
	            if(p.id === id_produit) {
	              bool = true;
	              prdt = p;
	            }
	          });

	         return bool ? {find: true, produit: prdt} : {find: false, produit: null};
        }
	};

});


//service de stockage de valeur
angular.module('webcatalogue').factory('ValuesProvider', function() {
	return {
		getModalObjectSuccessAjout : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalAjoutSuccess',
				title : 'Création d\'un nouveau produit',
				message : 'Votre produit a bien été ajouté'
			};
		},
		getModalObjectWarningPanier : function() {
			return {
				success : false,
				warning : true,
				id : 'printerModalListProduit',
				title : 'Supression du produit impossible',
				message : 'Le produit que vous souhaitez supprimer est actuellement dans le panier.'
			};
		},
		getModalObjectSuccessSuppressionProduit : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalListProduit',
				title : 'Supression du produit',
				message : 'Le produit a bien été supprimé.'
			};
		}
	};

});*/
'use strict';

/* angular.module('webcatalogue').controller('AppController' , ['Produit', 'Vente', '$element', 'APIService', 'AppTools', function (Produit, Vente, $element, APIService, AppTools) {
  var ctrl = vm;
  this.currentAction = 'produits';
  this.produit = new Produit(); 
  this.myManagerComponent;
  this.panier = {
    vente: new Vente(),
    produits: []
  };

  this.is_panier_clicked = false;

  this.changeAction = function(newAction) {
    ctrl.currentAction = newAction;
  };

  this.setMyManagerComponent = function(component) {
    this.myManagerComponent = component;
  };

  this.showPanier = function() {
    ctrl.is_panier_clicked = !ctrl.is_panier_clicked;
  };

  this.addQuantite = function(product) {
    console.log(product);

    //véfifier quantité en stock

    var new_quantite = product.getQuantiteVendue() + 1;
    if(new_quantite <= product.getStockQuantite()) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    ctrl.recalculerPrixTotal();
  };

  this.removeQuantite = function(product) {
    var new_quantite = product.getQuantiteVendue() - 1;
    if(new_quantite > 0) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    ctrl.recalculerPrixTotal();
  };

  this.removeFromPanier = function(index){
    var suppr = new Produit(ctrl.panier.produits[index]);
    ctrl.panier.produits.splice(index,1);

    //mettre a jour le prix total de la vente
    ctrl.recalculerPrixTotal();

    ctrl.showPanier();
  };

  this.createVente = function() {
    $element.find('#createVenteBtn').disabled = true;
    ctrl.panier.vente.setDate(AppTools.convertStrToDate(''));
    ctrl.updateStockQuantiteProduits();
    console.log(ctrl.panier);
    APIService.createVente(ctrl.panier).then(function successful(response) {
      ctrl.panier = {
        vente: new Vente(),
        produits: []
      };
      ctrl.showPanier();

      //recharger le tableau des produits
      ctrl.myManagerComponent.listeProduitComponent.loadProduits();

    });
  };

  this.recalculerPrixTotal = function() {
    ctrl.panier.vente.prix_total = 0;
    ctrl.panier.produits.forEach(function(product) {
      ctrl.panier.vente.prix_total += (product.quantite_vendue * parseFloat(product.prix_valeur));
    });
  };

  this.updateStockQuantiteProduits = function() {
    ctrl.panier.produits.forEach(function(product) {
      product.setStockQuantite(product.stock_quantite - product.quantite_vendue);
    });
  };




 }]);*/


function Ctrl ($scope, Produit, Vente, AppTools, APIService) {
  var vm = this;
  vm.currentAction = 'produits';
  vm.produit = new Produit(); 
  vm.myManagerComponent;
  vm.panier = {
    vente: new Vente(),
    produits: []
  };

  vm.is_panier_clicked = false;

  vm.changeAction = function(newAction) {
    //console.log("ok");
    vm.currentAction = newAction;
  };

  vm.setMyManagerComponent = function(component) {
    vm.myManagerComponent = component;
  };

  vm.showPanier = function() {
    vm.is_panier_clicked = !vm.is_panier_clicked;
  };

  vm.addQuantite = function(product) {
    //console.log(product);

    //véfifier quantité en stock

    var new_quantite = product.getQuantiteVendue() + 1;
    if(new_quantite <= product.getStockQuantite()) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    vm.recalculerPrixTotal();
  };

  vm.removeQuantite = function(product) {
    var new_quantite = product.getQuantiteVendue() - 1;
    if(new_quantite > 0) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    vm.recalculerPrixTotal();
  };

  vm.removeFromPanier = function(index){
    //var suppr = new Produit(vm.panier.produits[index]);
    vm.panier.produits.splice(index,1);

    //mettre a jour le prix total de la vente
    vm.recalculerPrixTotal();

    vm.showPanier();
  };

  vm.createVente = function() {
    vm.panier.vente.setDate(AppTools.convertStrToDate(''));
    vm.updateStockQuantiteProduits();
    //console.log(vm.panier);
    APIService.createVente(vm.panier).then(function successful() {
      vm.panier = {
        vente: new Vente(),
        produits: []
      };
      vm.showPanier();

      //recharger le tableau des produits
      vm.myManagerComponent.listeProduitComponent.loadProduits();

    });
  };

  vm.recalculerPrixTotal = function() {
    vm.panier.vente.prix_total = 0;
    vm.panier.produits.forEach(function(product) {
      vm.panier.vente.prix_total += (product.quantite_vendue * parseFloat(product.prix_valeur));
    });
  };

  vm.updateStockQuantiteProduits = function() {
    vm.panier.produits.forEach(function(product) {
      product.setStockQuantite(product.stock_quantite - product.quantite_vendue);
    });
  };
}

Ctrl.$inject = ['$scope', 'Produit', 'Vente', 'AppTools', 'APIService'];

angular
  .module('webcatalogue')
  .controller('AppController', Ctrl);
'use strict';
angular.module('webcatalogue').component('printerModal', {
    template:  
    [
      '<div id="{{$ctrl.modalObject.id}}" class="modal">',
        '<div class="modal-dialog">',
         '<div class="modal-content">',
            '<div class="modal-header">',
              '<h5 class="modal-title">{{$ctrl.modalObject.title}}</h5>',
              '<button type="button" class="close" ng-click="$ctrl.hide()" data-dismiss="modal" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<div class="alert" ng-class="{\'alert-success\' : $ctrl.modalObject.success, \'alert-warning\' : $ctrl.modalObject.warning}" role="alert">',
                    '{{ $ctrl.modalObject.message }}',
                '</div>',
            '</div>',
            '<div class="modal-footer">',
              '<button type="button" ng-click="$ctrl.hide()" class="btn btn-primary">OK</button>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      modalObject: '<'
    },
    controller: ['$timeout', '$document',  function($timeout, $document) {
       var ctrl = this;
       this.modal = null;
      
      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
      };

      this.show = function() {
        if(ctrl.modal === null) {
          var id = ctrl.modalObject.id;
          ctrl.modal = $document[0].getElementById(id);
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 
      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

    }]
  
});
angular.module('webcatalogue').component('customModalVenteReview', {
    template:  
    [
      '<div id="modalVenteReview" class="modal">',
         '<div class="modal-content avoid-full">',
            '<div class="modal-header">',
              '<h5 class="modal-title">Récapitulatif de la vente n° <b>{{$ctrl.vente.id}}</b></h5>',
              '<button type="button" class="close closeModal" ng-click="$ctrl.hide()" data-dismiss="modal" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<div id="cd-cart" style="width: 100% !important; position:relative !important; padding-top:0px !important;">',
                  '<h2>Produit(s) acheté(s)</h2>',
                    '<ul class="cd-cart-items">',
                      '<li ng-repeat="product in $ctrl.vente.panier">',
                      '<span class="cd-qty">{{product.quantite_vendue}}x</span> {{product.nom}} <span class="badge badge-warning" ng-show="product.disponible == 0">Indisponible</span>',
                      '<div class="cd-price">{{product.prix_valeur}} €</div>',
                      '</li>',
                    '</ul>',
                    '<div class="cd-cart-total">',
                      '<p>Total <span>{{$ctrl.vente.prix_total}} €</span></p>',
                    '</div>',
                '</div>',
            '</div>',
          '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      vente: '<'
    },
    controller: ['$timeout', '$document', function($timeout, $document) {
       var ctrl = this;
       this.modal = null;

      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
      };

     this.show = function() {
        if(ctrl.modal === null) {
          ctrl.modal = $document[0].getElementById("modalVenteReview");
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 
      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

    }]
  
});

angular.module('webcatalogue').component('customModalVente', {
    template:  
    [
      '<div id="modalVente" class="modal">',
        '<div class="modal-dialog">',
         '<div class="modal-content">',
            '<div class="modal-header">',
              '<h5 class="modal-title">Choisir la quantité pour ce produit</h5>',
              '<button type="button" class="close" ng-click="$ctrl.hide()" data-dismiss="modal" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<div class="col-xs-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text" >Quantité désirée</span>',
                    '</div>',
                        '<input type="number" ng-model="$ctrl.quantite"  class="form-control"  />',
                  '</div>',
                '</div>',
            '</div>',
            '<div class="modal-footer">',
              '<button type="button" ng-click="$ctrl.save()" ng-disabled="$ctrl.produit.stock_quantite < $ctrl.quantite || $ctrl.quantite <=0" class="btn btn-primary">Ajouter au panier</button>',
              '<button type="button" class="btn btn-warning" ng-click="$ctrl.hide()" data-dismiss="modal">Annuler</button>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      onSave: '&',
      produit: '<'
    },
    controller: ['$document', '$timeout', function($document, $timeout) {
       var ctrl = this;
       this.modal = null;
       this.formUpdateProduitComponent;
       this.quantite;
      
      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
        ctrl.quantite = parseInt("0");

      };


      this.save = function() {
        $timeout(function() {
          //mettre à jour la quantité vendue
          ctrl.produit.setQuantiteVendue(ctrl.produit.quantite_vendue + ctrl.quantite);

          //update le stock
          //var new_stock= ctrl.produit.stock_quantite - ctrl.quantite;
          //ctrl.produit.setStockQuantite(new_stock);
          
          ctrl.onSave({produit: ctrl.produit});
        });
        ctrl.hide();
       
      };

      this.show = function() {
        ctrl.quantite = 0;
        if(ctrl.modal === null) {

          ctrl.modal = $document[0].getElementById("modalVente");
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 

      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

    }]
  
});


angular.module('webcatalogue').component('customModal', {
    template:  
    [
      '<div id="modal" class="modal">',
        '<div class="modal-dialog modal-600">',
         '<div class="modal-content">',
            '<div class="modal-header">',
              '<h5 class="modal-title">{{$ctrl.title}}</h5>',
              '<button type="button" class="close" data-dismiss="modal" ng-click="$ctrl.hide()" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<form-update-produit on-init="$ctrl.setFormUpdateProduit(form)" produit="$ctrl.produit"></form-update-produit>',
            '</div>',
            '<div class="modal-footer">',
              '<button type="button" ng-click="$ctrl.save()" ng-disabled="!$ctrl.produit.isCompleted()" class="btn btn-primary">Enregistrer les changements</button>',
              '<button type="button" class="btn btn-warning" ng-click="$ctrl.hide()" data-dismiss="modal">Annuler</button>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      title: '@',
      onSave: '&',
      produit: '<'
    },
    controller: ['$document', '$timeout', 'Produit', 'APIService', 'AppTools', function($document, $timeout, Produit, APIService, AppTools) {
       var ctrl = this;
       this.modal = null;
       this.formUpdateProduitComponent;
      
      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
      };

      this.setFormUpdateProduit = function(form) {
        ctrl.formUpdateProduitComponent = form;
      };

      this.save = function() {
        ctrl.formatePrixDateDebut();
        ctrl.formatePrixDateFin();
        ctrl.onSave({produit: ctrl.produit});
      };

      this.show = function() {
        ctrl.quantite = 0;
        if(ctrl.modal === null) {
          ctrl.modal = $document[0].getElementById("modal");
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 

      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

      this.formatePrixDateDebut = function() {
          ctrl.produit.prix_date_debut_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_debut);
      };

       this.formatePrixDateFin = function() {
          ctrl.produit.prix_date_fin_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_fin);
      };

    }]
  
});


angular.module('webcatalogue').component('componentManager', {
    template:   
  [
    '<form-ajout-produit produit="$ctrl.produit" on-send="$ctrl.registerProduit(produit)" ng-show="$ctrl.action==\'ajout-produit\'" on-init="$ctrl.setFormAjoutProduit(form)"></form-ajout-produit>',
    '<list-produits panier="$ctrl.panier" ng-show="$ctrl.action==\'produits\'" on-init="$ctrl.setListeProduits(grid)"></list-produits>',
    '<list-ventes ng-show="$ctrl.action==\'ventes\'" on-init="$ctrl.setListeVente(grid)"></list-ventes>'
  ].join(''),
    bindings: {
        action: '<',
        onInit: '&',
        produit: '<',
        panier: '='
    },
    controller: ['$element', '$timeout', 'Produit', 'APIService',  function($element, $timeout, Produit, APIService) {
       var ctrl = this;
       this.produit;
       this.formAjoutProduitComponent;
       this.listeProduitComponent;
       this.listeVenteComponenent;

       this.setFormAjoutProduit = function(form) {
          ctrl.formAjoutProduitComponent = form;
       };

       this.setListeProduits = function(grid) {
          ctrl.listeProduitComponent = grid;
       };

       this.setListeVente = function(grid) {
          ctrl.listeVenteComponenent = grid;
       };


       this.$onInit = function() {
          ctrl.onInit({component: ctrl});
       };

       this.$onChanges = function() {
          // console.log(ctrl.produit);
          
          //Quand l'action change, je recharge le tableau des produits si l'utilisateur a cliqué sur le menu 'liste des produits'
          if(ctrl.action == "produits")
              $timeout(function() {
                ctrl.listeProduitComponent.loadProduits();
              });
          else if(ctrl.action == "ventes")
            $timeout(function() {
                ctrl.listeVenteComponenent.loadVentes();
            });
       };

       this.registerProduit = function() {
          APIService.sendProduitToBdd(ctrl.produit).then(function success() {
              //Modal
              $timeout(function() {
                  ctrl.formAjoutProduitComponent.printModal();
                  ctrl.formAjoutProduitComponent.resetForm();
              }, 300);
             
          });
       };

    }]
});


//Formulaire d'ajout d'un produit
angular.module('webcatalogue').component('formAjoutProduit', {
    template:   
    [
      '<printer-modal on-init="$ctrl.setPrinterModal(modal)" modal-object="$ctrl.printerModalObject"></printer-modal>',
      '<h3 class="text-center" style="margin-bottom: 20px">Formulaire d\'ajout d\'un nouveau produit</h3>',
       '<form id="formAjout">',
          '<div class="row">',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Nom</span>',
                    '</div>',
                        '<input type="text" ng-class="{\'is-invalid\' : $ctrl.produit.nom == \'\'}" ng-model="$ctrl.produit.nom" class="form-control">',
                  '</div>',
                '</div>',

                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Catégorie</span>',
                    '</div>',
                    '<select ng-model="$ctrl.produit.categorie_id" class="form-control form-control-sm">',
                        '<option ng-selected="selected" value="-1">Sélectionner une catégorie</option>',
                        '<option ng-repeat="c in $ctrl.categories" value="{{c.value}}">{{c.libelle}}</option>',
                    '</select>',
                  '</div>',
                '</div>',

                '<div class="col-md-4" style="padding:10px">',
                      '<div class="input-group input-group-sm">',
                        '<div class="input-group-prepend">',
                          '<span class="input-group-text">Stock</span>',
                        '</div>',
                            '<input type="number" ng-class="{\'is-invalid\' : $ctrl.produit.stock_quantite < 0}" ng-model="$ctrl.produit.stock_quantite" class="form-control">',
                      '</div>',
                    '</div>',
                '</div>',
            '</div>',

            '<div class="row">',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Prix</span>',
                    '</div>',
                        '<input type="text" ng-class="{\'is-invalid\' : $ctrl.produit.prix_valeur < 0 || $ctrl.produit.prix_valeur.search($ctrl.regexNumber) > -1}" ng-model="$ctrl.produit.prix_valeur" class="form-control">',
                  '</div>',
                '</div>',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date début</span>',
                    '</div>',
                        '<input type="date" ng-change="$ctrl.formatePrixDateDebut()" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_debut}" ng-model="$ctrl.produit.prix_date_debut" class="form-control">',
                  '</div>',
                '</div>',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date fin</span>',
                    '</div>',
                        '<input type="date" ng-change="$ctrl.formatePrixDateFin()" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_fin}" ng-model="$ctrl.produit.prix_date_fin" class="form-control">',
                  '</div>',
                '</div>',
            '</div>',

             '<div class="row">',
                '<div class="col-md-12" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend ">',
                      '<span class="input-group-text">Description</span>',
                    '</div>',
                    '<textarea ng-model="$ctrl.produit.description" rows="5" cols="55">',
                    '</textarea>',
                  '</div>',
                '</div>',
            '</div>',

            '<div class="row">',
              '<div class="col-md"12" style="text-align:center">',
                  '<button type="button" ng-disabled="!$ctrl.produit.isCompleted()" class="btn btn-success" ng-click="$ctrl.sendProduit()">Envoyer</button>',
              '</div>',
            '</div>',
        '</form>'
    ].join(''),
    bindings: {
        onInit: '&',
        produit: '<',
        onSend: '&'
    },
    controller: ['$element', '$timeout', 'Produit', 'AppTools', 'APIService', 'ValuesProvider', function($element, $timeout, Produit, AppTools, APIService, ValuesProvider) {
       var ctrl = this;
       this.categories = [];
       this.regexNumber;
       this.printerModal;
       this.printerModalObject = {};

       this.$onInit = function() {
          ctrl.onInit({form: ctrl});
          APIService.getCategories().then(function(response) {
              ctrl.categories = response.data;
          });
          ctrl.regexNumber = /[A-Z;a-z;]/g;

          $timeout(function() {
            $element.find("input.form-control").css("width", 330);
          });
       };

       this.setPrinterModal = function(modal) {
          this.printerModal = modal;
       };
       
       this.formatePrixDateDebut = function() {
          ctrl.produit.prix_date_debut_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_debut);
       };

       this.formatePrixDateFin = function() {
          ctrl.produit.prix_date_fin_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_fin);
       };

       this.sendProduit = function() {
          ctrl.onSend({produit: ctrl.produit});           
       };

       this.resetForm = function() {
          ctrl.produit = new Produit();
       };

       this.printModal = function() {
          ctrl.printerModalObject = ValuesProvider.getModalObjectSuccessAjout();
          $timeout(function() {
              ctrl.printerModal.show();
          });
       };

       

    }]
});

//Formulaire de modification d'un produit -> appelé dans le modal "customModal"
angular.module('webcatalogue').component('formUpdateProduit', {
    template:   
    [
       '<form id="formUpdate">',
          '<div class="row">',
              '<div class="col-md-12" style="padding:10px">',
                '<div class="input-group input-group-sm">',
                  '<div class="input-group-prepend">',
                    '<span class="input-group-text">Identifiant</span>',
                  '</div>',
                      '<input type="text" ng-model="$ctrl.produit.id" class="form-control" disabled>',
                '</div>',
              '</div>',
            '</div>',
             '<div class="row">',
                  '<div class="col-md-12" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Nom</span>',
                      '</div>',
                          '<input type="text" ng-model="$ctrl.produit.nom" class="form-control">',
                    '</div>',
                  '</div>',
               '</div>',
               '<div class="row">',
                  '<div class="col-md-12" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Catégorie</span>',
                      '</div>',
                      '<select ng-model="$ctrl.produit.categorie_id" class="form-control form-control-sm">',
                          '<option ng-selected="selected" value="-1">Sélectionner une catégorie</option>',
                          '<option ng-repeat="c in $ctrl.categories" value="{{c.value}}">{{c.libelle}}</option>',
                      '</select>',
                    '</div>',
                  '</div>',
                '</div>',
                '<div class="row">',
                  '<div class="col-md-6" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Stock</span>',
                      '</div>',
                          '<input type="number" ng-class="{\'is-invalid\' : $ctrl.produit.stock_quantite < 0}" ng-model="$ctrl.produit.stock_quantite" class="form-control">',
                    '</div>',
                  '</div>',
                  '<div class="col-md-6" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Prix</span>',
                      '</div>',
                          '<input type="text" ng-class="{\'is-invalid\' : $ctrl.produit.prix_valeur < 0 || $ctrl.produit.prix_valeur.search($ctrl.regexNumber) > -1}" ng-model="$ctrl.produit.prix_valeur" class="form-control">',
                    '</div>',
                  '</div>',
                '</div>',
                '<div class="row">',
                  '<div class="col-md-12" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend ">',
                        '<span class="input-group-text">Description</span>',
                      '</div>',
                      '<textarea ng-model="$ctrl.produit.description" rows="5" cols="52">',
                      '</textarea>',
                    '</div>',
                  '</div>',
              '</div>',
              '<div class="row">',
                '<div class="col-md-6" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date début</span>',
                    '</div>',
                        '<input type="date" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_debut}" ng-model="$ctrl.produit.prix_date_debut" class="form-control">',
                  '</div>',
                '</div>',
                '<div class="col-md-6" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date fin</span>',
                    '</div>',
                        '<input type="date" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_fin}" ng-model="$ctrl.produit.prix_date_fin" class="form-control">',
                  '</div>',
                '</div>',
            '</div>',
        '</form>'
    ].join(''),
    bindings: {
        onInit: '&',
        produit: '<',
        onSend: '&'
    },
    controller: ['$element', '$timeout', 'Produit', 'AppTools', 'APIService', function($element, $timeout, Produit, AppTools, APIService) {
       var ctrl = this;
       this.categories = [];
       this.regexNumber;

       this.$onInit = function() {
          ctrl.onInit({form: ctrl});
          APIService.getCategories().then(function(response) {
              ctrl.categories = response.data;
          });
          ctrl.regexNumber = /[A-Z;a-z;]/g;
         
       };
       

    }]
});

angular.module('webcatalogue').component('listProduits', {
    template:   
  [
   '<printer-modal on-init="$ctrl.setPrinterModal(modal)" modal-object="$ctrl.printerModalObject"></printer-modal>',
    '<custom-modal-vente on-init="$ctrl.setModalVenteComponent(modal)" produit="$ctrl.selectedProduit" on-save="$ctrl.alimenterPanier(produit)"></custom-modal-vente>',
    '<custom-modal produit="$ctrl.selectedProduit" title="Modification du produit" on-init="$ctrl.setModalComponent(modal)" on-save="$ctrl.updateProduit(produit)"></custom-modal>',
    '<table class="table col-md-12">',
        '<thead class="thead-light">',
            '<tr>',
                '<th scope="col"></th>',
                '<th scope="col">Id</th>',
                '<th scope="col">Nom</th>',
                '<th scope="col">Description</th>',
                '<th scope="col">Catégorie</th>',
                '<th scope="col">Mise à jour du stock</th>',
                '<th scope="col">Quantité en stock</th>',
                '<th scope="col">Prix</th>',
                '<th scope="col">Date début</th>',
                '<th scope="col">Date fin</th>',
            '</tr>',
        '</thead>',
        '<tbody>',
            '<tr ng-repeat="produit in $ctrl.produits" ng-class="{\'table-warning\' : produit.stock_quantite == 0}">',
                '<th scope="col">',
                  '<div class="btn-group">',
                    '<button type="button" style="margin-right:5px" ng-click="$ctrl.showModalEditing(produit)" class="btn btn-sm btn-warning">Modifier</button>',
                    '<button type="button" style="margin-right:5px" ng-click="$ctrl.addToKart(produit)" class="btn btn-sm btn-success">Ajouter au panier</button>',
                    '<button type="button" style="margin-right:5px" ng-click="$ctrl.removeProduit(produit)" class="btn btn-sm btn-danger">Supprimer</button>',
                  '</div>',
                '</th>',
                '<th scope="col">{{produit.id}}</th>',
                '<th scope="col">{{produit.nom}}</th>',
                '<th scope="col">{{produit.description}}</th>',
                '<th scope="col">{{produit.categorie_libelle}}</th>',
                '<th scope="col">{{$ctrl.formateDate(produit.stock_dernier_maj)}}</th>',
                '<th scope="col">{{produit.stock_quantite}} en stock</th>',
                '<th scope="col">{{produit.prix_valeur}} €</th>',
                '<th scope="col">{{$ctrl.formateDate(produit.prix_date_debut_formated)}}</th>',
                '<th scope="col">{{$ctrl.formateDate(produit.prix_date_fin_formated)}}</th>',
            '</tr>',
        '</tbody>',
    '</table>'
  ].join(''),
    bindings: {
        onInit: '&',
        panier: '='
    },
    controller: ['$element', '$timeout', 'Produit', 'AppTools', 'APIService', 'ValuesProvider',  function($element, $timeout, Produit, AppTools, APIService, ValuesProvider) {
      var ctrl = this;
      this.produits = [];
      this.modalComponent;
      this.modalVenteComponent;
      this.selectedProduit = {};
      this.printerModal;
      this.printerModalObject = {};

       this.$onInit = function() {
          ctrl.onInit({grid: ctrl});
       };

       this.setPrinterModal = function(modal) {
          ctrl.printerModal = modal;
       };

       this.updateProduit = function(produit) {

          //APIService
          APIService.updateProduit(produit).then(function success() {
            
            //Reload le tableau des produits
            $timeout(function() {
              ctrl.modalComponent.hide();
              ctrl.loadProduits();
            });

          });
          
       };

       this.setModalComponent = function(modal) {
          ctrl.modalComponent = modal;
       };

       this.setModalVenteComponent = function(modal) {
        ctrl.modalVenteComponent = modal;
       };

       this.loadProduits = function() {
          ctrl.produits = [];
          APIService.getProduits().then(function(response) {
              $timeout(function() {
                  response.data.forEach(function(object){
                     ctrl.produits.push(new Produit(object));
                  });
              });
          });
       };

       this.formateDate = function(date) {
          return AppTools.formateDateForGrid(date);
       };

       this.showModalEditing = function(produit) {

          ctrl.selectedProduit = new Produit(produit.formateToForm());
          $timeout(function() {
              ctrl.modalComponent.show();
          });
       };

       this.showModalVente = function() {
          $timeout(function() {
              ctrl.modalVenteComponent.show();
          });
       };

       this.addToKart = function(produit) {
          ctrl.selectedProduit = new Produit(produit);
          ctrl.showModalVente();
       };

       this.removeProduit = function(produit) {
            var found = {};
            found = AppTools.findProduitInPanier(ctrl.panier, produit.id);

            if(found.find) { //Message de warning : le produit est actuellement dans le panier
                ctrl.printerModalObject = ValuesProvider.getModalObjectWarningPanier();

                $timeout(function() {
                    ctrl.printerModal.show();
                });
            }
            else { //Suppression dans la bdd selon s'il appartient à une vente 
              APIService.deleteProduit(produit);
                ctrl.printerModalObject = ValuesProvider.getModalObjectSuccessSuppressionProduit();
                $timeout(function() {
                  ctrl.printerModal.show();
                  ctrl.loadProduits();
                });
            }

            
       };

       this.alimenterPanier = function(produit) {
          var found = {};
          found = AppTools.findProduitInPanier(ctrl.panier, produit.id);
          //console.log(found);         
          if(!found.find) {
              ctrl.panier.produits.push(produit);
              ctrl.panier.vente.setPrixTotal(ctrl.panier.vente.prix_total + (parseFloat(produit.prix_valeur) * parseInt(produit.quantite_vendue)));
          }
          else {
              found.produit.setQuantiteVendue(found.produit.quantite_vendue + produit.quantite_vendue);
              ctrl.panier.vente.setPrixTotal(ctrl.panier.vente.prix_total + (parseFloat(produit.prix_valeur) * parseInt(produit.quantite_vendue)));
          }
       };

    }]
});

angular.module('webcatalogue').component('listVentes', {
    template:   
    [
        '<custom-modal-vente-review on-init="$ctrl.setModalVenteReview(modal)" vente="$ctrl.selectedVente"></custom-modal-vente-review>',
        '<table class="table col-md-12">',
            '<thead class="thead-light">',
                '<tr>',
                    '<th scope="col"></th>',
                    '<th scope="col">Id</th>',
                    '<th scope="col">Date vente</th>',
                    '<th scope="col">Prix total</th>',
                    '<th scope="col">Nombre d\'articles vendues</th>',
                '</tr>',
            '</thead>',
            '<tbody>',
                '<tr ng-repeat="vente in $ctrl.ventes">',
                    '<th scope="col">',
                      '<div class="btn-group">',
                        '<button type="button" class="btn btn-sm btn-info" ng-click="$ctrl.loadVente(vente)">',
                          'Voir le détail',
                        '</button>',
                      '</div>',
                    '</th>',
                    '<th scope="col">{{vente.id}}</th>',
                    '<th scope="col">{{$ctrl.formateDate(vente.date)}}</th>',
                    '<th scope="col">{{vente.prix_total}}</th>',
                    '<th scope="col">{{vente.total_produits}}</th>',
                '</tr>',
            '</tbody>',
        '</table>'
    ].join(''),
    bindings: {
      onInit: '&'
    },
    controller: ['$element', '$timeout', 'Produit','Vente', 'AppTools', 'APIService',  function($element, $timeout, Produit, Vente, AppTools, APIService) {
       var ctrl = this;
       this.ventes = [];
       this.selectedVente;
       this.modalVenteReview;

       this.$onInit = function() {
          ctrl.onInit({grid: ctrl});
       };

       this.setModalVenteReview = function(modal) {
        this.modalVenteReview = modal;
       };

       this.loadVentes = function() {
          ctrl.ventes = [];
          APIService.getVentes().then(function(response) {
              $timeout(function() {
                  response.data.forEach(function(object){
                     ctrl.ventes.push(new Vente(object));
                  });
              });
          });
       };

       this.loadVente = function(vente) {
          APIService.getVente(vente.id).then(function(response) {
              ctrl.selectedVente = new Vente(vente);
              ctrl.selectedVente.panier = [];
              response.data.forEach(function(produit) {
                ctrl.selectedVente.panier.push(new Produit(produit));
              });
              //console.log(ctrl.selectedVente);
              ctrl.modalVenteReview.show();
          });
       };

        this.formateDate = function(date) {
          return AppTools.formateDateForGrid(date);
       };


    }]
});

angular.module('webcatalogue').factory('Produit', [function() {

	function Produit(data) {
		if(data) {
			this.setData(data);
		}
	}

	Produit.prototype = {

		//Attributs d'un produit
		id: null,
		nom: null,
		description: null,
		categorie_id: null,
		categorie_libelle: null,
		stock_id: null,
		stock_dernier_maj:null,
		stock_quantite:null,
		prix_id:null,
		prix_valeur:null,
		prix_date_debut:null,
		prix_date_debut_formated:null,
		prix_date_fin:null,
		prix_date_fin_formated: null,
		quantite_vendue:null,
		ventes:null,
		disponible: null,


		//Hydratation de l'objet
		setData: function(data) {
			angular.extend(this, data);
			this.setIntegerValues();
		},

		

		//Getters
		getId: function() {
			return this.id;
		},
		getNom: function() {
			return this.nom;
		},
		getDescription: function() {
			return this.description;
		},
		getCategorieId: function() {
			return this.categorie_id;
		},
		getCategorieLibelle: function() {
			return this.categorie_libelle;
		},
		getStockId: function() {
			return this.stock_id;
		},
		getStockDernierMaj: function() {
			return this.stock_dernier_maj;
		},
		getStockQuantite: function() {
			return this.stock_quantite;
		},
		getPrixId: function() {
			return this.prix_id;
		},
		getPrixValeur: function() {
			return this.prix_valeur;
		},
		getPrixDateDebut: function() {
			return this.prix_date_debut;
		},
		getPrixDateDebutFormated: function() {
			return this.prix_date_debut_formated;
		},
		getPrixDateFin: function() {
			return this.prix_date_fin;
		},
		getPrixDateFinFormated: function() {
			return this.prix_date_fin_formated;
		}, 
		getQuantiteVendue: function() {
			return this.quantite_vendue;
		},
		getDisponible: function() {
			return this.disponible;
		},

		//Setters
		setId: function(id) {
			this.id = id;
		},
		setNom: function(nom) {
			this.nom = nom;
		},
		setDescription: function(description) {
			this.description = description;
		},
		setCategorieId: function(categorie_id) {
			this.categorie_id = categorie_id;
		},
		setCategorieLibelle: function(categorie_libelle) {
			this.categorie_libelle = categorie_libelle;
		},
		setStockId: function(stock_id) {
			this.stock_id = stock_id;
		},
		setStockDernierMaj: function(stock_dernier_maj) {
			this.stock_dernier_maj = stock_dernier_maj;
		},
		setStockQuantite: function(stock_quantite) {
			this.stock_quantite = stock_quantite;
		},
		setPrixId: function(prix_id) {
			this.prix_id = prix_id;
		},
		setPrixValeur: function(prix_valeur) {
			this.prix_valeur = prix_valeur;
		},
		setPrixDateDebut: function(prix_date_debut) {
			this.prix_date_debut = prix_date_debut;
		},
		setPrixDateFin: function(prix_date_fin) {
			this.prix_date_fin = prix_date_fin;
		},
		setQuantiteVendue: function(quantite_vendue) {
			this.quantite_vendue = quantite_vendue;
		},
		setDisponible: function(dispo) {
			this.disponible = dispo;
		},

		setIntegerValues: function() {
			if(this.quantite_vendue) this.quantite_vendue = parseInt(this.quantite_vendue);
			if(this.stock_quantite) this.stock_quantite = parseInt(this.stock_quantite);

		},


		isCompleted: function() {
			var bool = (this.getNom() !== null && this.getDescription() !== null && this.getCategorieId() !== null && this.getCategorieId() !== "-1");

			bool = bool && (this.getStockQuantite() !== null && Number.isInteger(this.getStockQuantite()));

			bool = bool && (this.getPrixValeur() !== null && this.getPrixValeur().search(/[A-Z;a-z;]/g) < 0);


			bool = bool && (this.getPrixDateDebutFormated() !== null && this.getPrixDateFinFormated() !== null);


			return bool;
		},

		formateToForm: function() {
			//this.setPrixValeur(parseInt(this.getPrixValeur()));
			this.setStockQuantite(parseInt(this.getStockQuantite()));

			var debut = new Date(this.getPrixDateDebutFormated());
			var fin = new Date(this.getPrixDateFinFormated());

			this.setPrixDateDebut(debut);
			this.setPrixDateFin(fin);

			// this.setPrixDateDebut(("0" + debut.getDate()).slice(-2) + "/" + ("0" + (debut.getMonth() + 1)).slice(-2) + "/" + debut.getFullYear());
			// this.setPrixDateFin(("0" + fin.getDate()).slice(-2) + "/" + ("0" + (fin.getMonth() + 1)).slice(-2) + "/" + fin.getFullYear());

			return this;
		}
	};
	return Produit;
}]);;
angular.module('webcatalogue').factory('Vente', [function() {

	function Vente(data) {
		if(data) {
			this.setData(data);
		}
	}

	Vente.prototype = {

		//Attributs d'un produit
		id: null,
		date: null,
		prix_total: null,
		panier:null, //liste des produits avec la quantité vendue
		valider: null,
		total_produits: null,


		//Hydratation de l'objet
		setData: function(data) {
			angular.extend(this, data);
		},

		//Getters
		getId: function() {
			return this.id;
		},
		getDate: function() {
			return this.date;
		},
		getPrixTotal: function() {
			return this.prix_total;
		},
		getPanier: function() {
			return this.panier;
		},

		getValider: function() {
			return this.valider;
		},

		getTotalArticles: function() {
			return this.total_produits;
		},

		//Setters
		setId: function(id) {
			this.id = id;
		},
		setDate: function(date) {
			this.date = date;
		},
		setPrixTotal: function(prix_total) {
			this.prix_total = prix_total;
		},
		setPanier: function(panier) {
			this.panier = panier;
		},
		setValider: function(valider) {
			this.valider = valider;
		}

	};
	return Vente;
}]);