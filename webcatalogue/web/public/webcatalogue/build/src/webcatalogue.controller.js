'use strict';

/* angular.module('webcatalogue').controller('AppController' , ['Produit', 'Vente', '$element', 'APIService', 'AppTools', function (Produit, Vente, $element, APIService, AppTools) {
  var ctrl = vm;
  this.currentAction = 'produits';
  this.produit = new Produit(); 
  this.myManagerComponent;
  this.panier = {
    vente: new Vente(),
    produits: []
  };

  this.is_panier_clicked = false;

  this.changeAction = function(newAction) {
    ctrl.currentAction = newAction;
  };

  this.setMyManagerComponent = function(component) {
    this.myManagerComponent = component;
  };

  this.showPanier = function() {
    ctrl.is_panier_clicked = !ctrl.is_panier_clicked;
  };

  this.addQuantite = function(product) {
    console.log(product);

    //véfifier quantité en stock

    var new_quantite = product.getQuantiteVendue() + 1;
    if(new_quantite <= product.getStockQuantite()) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    ctrl.recalculerPrixTotal();
  };

  this.removeQuantite = function(product) {
    var new_quantite = product.getQuantiteVendue() - 1;
    if(new_quantite > 0) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    ctrl.recalculerPrixTotal();
  };

  this.removeFromPanier = function(index){
    var suppr = new Produit(ctrl.panier.produits[index]);
    ctrl.panier.produits.splice(index,1);

    //mettre a jour le prix total de la vente
    ctrl.recalculerPrixTotal();

    ctrl.showPanier();
  };

  this.createVente = function() {
    $element.find('#createVenteBtn').disabled = true;
    ctrl.panier.vente.setDate(AppTools.convertStrToDate(''));
    ctrl.updateStockQuantiteProduits();
    console.log(ctrl.panier);
    APIService.createVente(ctrl.panier).then(function successful(response) {
      ctrl.panier = {
        vente: new Vente(),
        produits: []
      };
      ctrl.showPanier();

      //recharger le tableau des produits
      ctrl.myManagerComponent.listeProduitComponent.loadProduits();

    });
  };

  this.recalculerPrixTotal = function() {
    ctrl.panier.vente.prix_total = 0;
    ctrl.panier.produits.forEach(function(product) {
      ctrl.panier.vente.prix_total += (product.quantite_vendue * parseFloat(product.prix_valeur));
    });
  };

  this.updateStockQuantiteProduits = function() {
    ctrl.panier.produits.forEach(function(product) {
      product.setStockQuantite(product.stock_quantite - product.quantite_vendue);
    });
  };




 }]);*/


function Ctrl ($scope, Produit, Vente, AppTools, APIService) {
  var vm = this;
  vm.currentAction = 'produits';
  vm.produit = new Produit(); 
  vm.myManagerComponent;
  vm.panier = {
    vente: new Vente(),
    produits: []
  };

  vm.is_panier_clicked = false;

  vm.changeAction = function(newAction) {
    //console.log("ok");
    vm.currentAction = newAction;
  };

  vm.setMyManagerComponent = function(component) {
    vm.myManagerComponent = component;
  };

  vm.showPanier = function() {
    vm.is_panier_clicked = !vm.is_panier_clicked;
  };

  vm.addQuantite = function(product) {
    //console.log(product);

    //véfifier quantité en stock

    var new_quantite = product.getQuantiteVendue() + 1;
    if(new_quantite <= product.getStockQuantite()) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    vm.recalculerPrixTotal();
  };

  vm.removeQuantite = function(product) {
    var new_quantite = product.getQuantiteVendue() - 1;
    if(new_quantite > 0) {
      product.setQuantiteVendue(new_quantite);
    }

    //update le prix total de la vente
    vm.recalculerPrixTotal();
  };

  vm.removeFromPanier = function(index){
    //var suppr = new Produit(vm.panier.produits[index]);
    vm.panier.produits.splice(index,1);

    //mettre a jour le prix total de la vente
    vm.recalculerPrixTotal();

    vm.showPanier();
  };

  vm.createVente = function() {
    vm.panier.vente.setDate(AppTools.convertStrToDate(''));
    vm.updateStockQuantiteProduits();
    //console.log(vm.panier);
    APIService.createVente(vm.panier).then(function successful() {
      vm.panier = {
        vente: new Vente(),
        produits: []
      };
      vm.showPanier();

      //recharger le tableau des produits
      vm.myManagerComponent.listeProduitComponent.loadProduits();

    });
  };

  vm.recalculerPrixTotal = function() {
    vm.panier.vente.prix_total = 0;
    vm.panier.produits.forEach(function(product) {
      vm.panier.vente.prix_total += (product.quantite_vendue * parseFloat(product.prix_valeur));
    });
  };

  vm.updateStockQuantiteProduits = function() {
    vm.panier.produits.forEach(function(product) {
      product.setStockQuantite(product.stock_quantite - product.quantite_vendue);
    });
  };
}

Ctrl.$inject = ['$scope', 'Produit', 'Vente', 'AppTools', 'APIService'];

angular
  .module('webcatalogue')
  .controller('AppController', Ctrl);