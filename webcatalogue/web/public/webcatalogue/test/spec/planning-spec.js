'use strict';

describe('Planning des cycles sous AngularJS', function () {
    var scope, createController;

    beforeEach(angular.mock.module('planning'));
    beforeEach(angular.mock.inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();

        createController = function () {
            return $controller('PlanningController', {
                '$scope': scope
            })
        };
    }));

    it('Test à créer', angular.mock.inject(function () {
        createController();

        expect(scope.planning).toBe(true);
    }));
});