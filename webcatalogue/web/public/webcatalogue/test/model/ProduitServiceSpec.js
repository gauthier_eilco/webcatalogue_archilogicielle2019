describe('Produit service', function() {
  var $scope = null;
  var ctrl = null;
  var Produit, httpBackend, resource, produit;

  //you need to indicate your module in a test
  beforeEach(module('webcatalogue'));

  beforeEach(inject(function($rootScope, $controller, $injector, _Produit_) {
    $scope = $rootScope.$new();
    httpBackend = $injector.get('$httpBackend');
    Produit = _Produit_;

    ctrl = $controller('AppController', {
      $scope: $scope
    });
  }));
  
  afterEach(function() {
    httpBackend.verifyNoOutstandingExpectation();
  });

  it("Produit devrait être une ressource", function() {
    expect(typeof Produit).toBe('function');
  });
  
  it('Attribut nom devrait être un String', function () {
    var json = {nom : 'John'};
    produit = new Produit(json);
    expect(typeof produit).toBe('object');
    expect(typeof produit.nom).toBe('string');
    expect(produit.nom).toEqual('John');
  });

  it('Méthode getNom devrait renvoyer un String', function () {
    var json = {nom : 'John'};
    produit = new Produit(json);
    expect(typeof produit.getNom()).toBe('string');
    expect(produit.getNom()).toEqual('John');
   
  });

  it('Méthode setNom devrait permettre de définir un string', function () {
    produit = new Produit();
    produit.setNom('John');
    expect(typeof produit.getNom()).toBe('string');
    expect(produit.getNom()).toEqual('John');
   
  });

  it('Méthode isCompleted devrait renvoyer false', function () {
    produit = new Produit({nom : "John"});
    expect(typeof produit.isCompleted()).toBe('boolean');
    expect(produit.isCompleted()).toEqual(false);
   
  });

  it('Méthode isCompleted devrait renvoyer false (2)', function () {
    produit = new Produit({
      nom : "John",
      description : "description",
      categorie_id : 1,
      stock_quantite : "aa",
      prix_valeur : "140",
      prix_date_debut_formated: '2010-10-10',
      prix_date_fin_formated : '2010-10-10'
    });
    expect(typeof produit.isCompleted()).toBe('boolean');
    expect(produit.isCompleted()).toEqual(false);
   
  });

   it('Méthode isCompleted devrait renvoyer true', function () {
    produit = new Produit({
      nom : "John",
      description : "description",
      categorie_id : 1,
      stock_quantite : 100,
      prix_valeur : "140",
      prix_date_debut_formated: '2010-10-10',
      prix_date_fin_formated : '2010-10-10'
    });
    expect(typeof produit.isCompleted()).toBe('boolean');
    expect(produit.isCompleted()).toEqual(true);
   
  });

   it('Méthode setIntegeValues devrait caster quantite_vendue et stock_quantite en number', function () {
    produit = new Produit({
      quantite_vendue : "12",
      stock_quantite : "140",
    });
    produit.setIntegerValues();
    expect(typeof produit.getQuantiteVendue()).toBe('number');
    expect(typeof produit.getStockQuantite()).toBe('number');
   
  });

});