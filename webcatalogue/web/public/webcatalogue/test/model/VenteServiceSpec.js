describe('Vente service', function() {
  var $scope = null;
  var ctrl = null;
  var Vente, httpBackend, resource, produit, vente;

  //you need to indicate your module in a test
  beforeEach(module('webcatalogue'));

  beforeEach(inject(function($rootScope, $controller, $injector, _Produit_, _Vente_) {
    $scope = $rootScope.$new();
    httpBackend = $injector.get('$httpBackend');
    Produit = _Produit_;
    Vente = _Vente_;

    ctrl = $controller('AppController', {
      $scope: $scope
    });
  }));
  
  afterEach(function() {
    httpBackend.verifyNoOutstandingExpectation();
  });

  it("Vente devrait être une ressource", function() {
    expect(typeof Vente).toBe('function');
  });
 

  it('Attribut Prix total devrait être un number', function () {
    var json = {prix_total: 142};
    vente = new Vente(json);
    expect(typeof vente).toBe('object');
    expect(typeof vente.getPrixTotal()).toBe('number');
    expect(vente.prix_total).toEqual(142);
  });

  it('Méthode setPrixTotal devrait permettre de définir un nombre', function () {
    vente = new Vente();
    vente.setPrixTotal(154);
    expect(vente.getPrixTotal()).toEqual(154);
   
  });

  it('Méthode getPanier devrait renvoyer un produit', function () {
    var produits = [
      new Produit({nom : "John"}), 
      new Produit({nom : "Pierre"})
    ];
    vente = new Vente();
    vente.setPanier(produits);
    expect(typeof vente.getPanier()).toBe('object');
    expect(vente.getPanier().length).toEqual(2);
    expect(typeof vente.getPanier()[0]).toBe('object');
    expect(vente.getPanier()[0].getNom()).toEqual('John');
  });

});