'use strict';

function apiService($http, ValuesProvider) {
	return {
        sendProduitToBdd: function(produit) {
	    	return $http({
	    		method:"POST",
	    		url:ValuesProvider.getApiUrl() + "produit",
	    		data:produit
	    	});
	  	},
	  	getCategories: function() {
	  		return $http({
		      	method: "GET",
		        url: ValuesProvider.getApiUrl() + "categories"
	    	});
	  	},
	  	getProduits: function() {
	  		return $http({
		      	method: "GET",
		        url: ValuesProvider.getApiUrl() + "produits"
	    	});
	  	},
	  	updateProduit: function(produit) {
	  		return $http({
	  			method: "PUT",
	  			url: ValuesProvider.getApiUrl() + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	deleteProduit: function(produit) {
	  		return $http({
	  			method: "POST",
	  			url: ValuesProvider.getApiUrl() + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	createVente: function(panier) {
	  		return $http({
		  		method: 'POST',
		  		url: ValuesProvider.getApiUrl() + "vente",
		  		data: {vente : panier.vente, produits: panier.produits}
		  	});
	  	},
	  	getVentes: function() {
	  		return $http({
	  			method: 'GET',
	  			url: ValuesProvider.getApiUrl() + "ventes"
	  		});
	  	},
	  	getVentesOfProduit: function(produit) {
	  		return $http({
	  			method: 'GET',
	  			url: ValuesProvider.getApiUrl() + "ventes/produit/"+ produit.id
	  		});
	  	},
	  	getVente: function(id) {
	  		return $http({
	  			method: 'GET',
	  			url: ValuesProvider.getApiUrl() + "vente/"+id
	  		});
	  	}
    };
}

apiService.$inject = ['$http', 'ValuesProvider'];

angular
  .module('webcatalogue')
  .factory('APIService', apiService);




function appTools() {
	return {
		convertStrToDate: function(str) { //Convertir une date de l'input vers un format date object
          	var date;
          	if(str != "") date = new Date(str);
          	else date = new Date();

            var mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2),
            hr = ("0" + date.getHours()).slice(-2),
            mn = ("0" + date.getMinutes()).slice(-2),
            sc = ("0" + date.getSeconds()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-") + ' ' + hr+':'+mn +':'+sc;
        },
        formateDateForGrid: function(date) { //Formater la date en version française parce que vive la France
        	var formated = new Date(date),
            mnth = ("0" + (formated.getMonth() + 1)).slice(-2),
            day = ("0" + formated.getDate()).slice(-2);
        	return [
	        		day,
	        	 "-", 
	        			mnth,
	        	 "-", 
	        			formated.getFullYear(), 
	        	 " ", 
	        			formated.getHours(), 
	        	 "h", 
	        			formated.getMinutes()
        	].join('');
        },
        findProduitInPanier: function(panier, id_produit) {
        	var bool = false;
        	var prdt;
        	//Vérifier que le produit n'existe pas déjà dans le panier
	         panier.produits.forEach(function(p) {
	            if(p.id === id_produit) {
	              bool = true;
	              prdt = p;
	            }
	          });

	         return bool ? {find: true, produit: prdt} : {find: false, produit: null};
        }
	};
}


appTools.$inject = [];

angular
  .module('webcatalogue')
  .factory('AppTools', appTools);

function valuesProvider($window) {
	return {
		getModalObjectSuccessAjout : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalAjoutSuccess',
				title : 'Création d\'un nouveau produit',
				message : 'Votre produit a bien été ajouté'
			};
		},
		getModalObjectWarningPanier : function() {
			return {
				success : false,
				warning : true,
				id : 'printerModalListProduit',
				title : 'Supression du produit impossible',
				message : 'Le produit que vous souhaitez supprimer est actuellement dans le panier.'
			};
		},
		getModalObjectSuccessSuppressionProduit : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalListProduit',
				title : 'Supression du produit',
				message : 'Le produit a bien été supprimé.'
			};
		},
		getApiUrl : function() {
			return $window.location.protocol + '//' + $window.location.host + '/api/';
		}
	};
}

valuesProvider.$inject = ['$window'];

angular
  .module('webcatalogue')
  .factory('ValuesProvider', valuesProvider);

/*//service API qui sert à communiquer avec l'API (notre back symfony)
angular.module('webcatalogue').factory('APIService', ['$http', 'API_URL_CATALOGUE', function ($http, API_URL_CATALOGUE) {
    return {
        sendProduitToBdd: function(produit) {
	    	return $http({
	    		method:"POST",
	    		url:API_URL_CATALOGUE + "produit",
	    		data:produit
	    	});
	  	},
	  	getCategories: function() {
	  		return $http({
		      	method: "GET",
		        url: API_URL_CATALOGUE + "categories"
	    	});
	  	},
	  	getProduits: function() {
	  		return $http({
		      	method: "GET",
		        url: API_URL_CATALOGUE + "produits"
	    	});
	  	},
	  	updateProduit: function(produit) {
	  		return $http({
	  			method: "PUT",
	  			url: API_URL_CATALOGUE + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	deleteProduit: function(produit) {
	  		return $http({
	  			method: "POST",
	  			url: API_URL_CATALOGUE + "produit/" + produit.id,
	  			data: produit
	  		});
	  	},
	  	createVente: function(panier) {
	  		return $http({
		  		method: 'POST',
		  		url: API_URL_CATALOGUE + "vente",
		  		data: {vente : panier.vente, produits: panier.produits}
		  	});
	  	},
	  	getVentes: function() {
	  		return $http({
	  			method: 'GET',
	  			url: API_URL_CATALOGUE + "ventes"
	  		});
	  	},
	  	getVentesOfProduit: function(produit) {
	  		return $http({
	  			method: 'GET',
	  			url: API_URL_CATALOGUE + "ventes/produit/"+ produit.id
	  		});
	  	},
	  	getVente: function(id) {
	  		return $http({
	  			method: 'GET',
	  			url: API_URL_CATALOGUE + "vente/"+id
	  		});
	  	}
    };
}]);


//service "outil" qui sert à tout et n'importe quoi
angular.module('webcatalogue').factory('AppTools', function() {
	return {
		convertStrToDate: function(str) { //Convertir une date de l'input vers un format date object
          	var date;
          	if(str != "") date = new Date(str);
          	else date = new Date();

            var mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2),
            hr = ("0" + date.getHours()).slice(-2),
            mn = ("0" + date.getMinutes()).slice(-2),
            sc = ("0" + date.getSeconds()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-") + ' ' + hr+':'+mn +':'+sc;
        },
        formateDateForGrid: function(date) { //Formater la date en version française parce que vive la France
        	var formated = new Date(date),
            mnth = ("0" + (formated.getMonth() + 1)).slice(-2),
            day = ("0" + formated.getDate()).slice(-2);
        	return [
	        		day,
	        	 "-", 
	        			mnth,
	        	 "-", 
	        			formated.getFullYear(), 
	        	 " ", 
	        			formated.getHours(), 
	        	 "h", 
	        			formated.getMinutes()
        	].join('');
        },
        findProduitInPanier: function(panier, id_produit) {
        	var bool = false;
        	var prdt;
        	//Vérifier que le produit n'existe pas déjà dans le panier
	         panier.produits.forEach(function(p) {
	            if(p.id === id_produit) {
	              bool = true;
	              prdt = p;
	            }
	          });

	         return bool ? {find: true, produit: prdt} : {find: false, produit: null};
        }
	};

});


//service de stockage de valeur
angular.module('webcatalogue').factory('ValuesProvider', function() {
	return {
		getModalObjectSuccessAjout : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalAjoutSuccess',
				title : 'Création d\'un nouveau produit',
				message : 'Votre produit a bien été ajouté'
			};
		},
		getModalObjectWarningPanier : function() {
			return {
				success : false,
				warning : true,
				id : 'printerModalListProduit',
				title : 'Supression du produit impossible',
				message : 'Le produit que vous souhaitez supprimer est actuellement dans le panier.'
			};
		},
		getModalObjectSuccessSuppressionProduit : function() {
			return {
				success : true,
				warning : false,
				id : 'printerModalListProduit',
				title : 'Supression du produit',
				message : 'Le produit a bien été supprimé.'
			};
		}
	};

});*/