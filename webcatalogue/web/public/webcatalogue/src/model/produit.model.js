angular.module('webcatalogue').factory('Produit', [function() {

	function Produit(data) {
		if(data) {
			this.setData(data);
		}
	}

	Produit.prototype = {

		//Attributs d'un produit
		id: null,
		nom: null,
		description: null,
		categorie_id: null,
		categorie_libelle: null,
		stock_id: null,
		stock_dernier_maj:null,
		stock_quantite:null,
		prix_id:null,
		prix_valeur:null,
		prix_date_debut:null,
		prix_date_debut_formated:null,
		prix_date_fin:null,
		prix_date_fin_formated: null,
		quantite_vendue:null,
		ventes:null,
		disponible: null,


		//Hydratation de l'objet
		setData: function(data) {
			angular.extend(this, data);
			this.setIntegerValues();
		},

		

		//Getters
		getId: function() {
			return this.id;
		},
		getNom: function() {
			return this.nom;
		},
		getDescription: function() {
			return this.description;
		},
		getCategorieId: function() {
			return this.categorie_id;
		},
		getCategorieLibelle: function() {
			return this.categorie_libelle;
		},
		getStockId: function() {
			return this.stock_id;
		},
		getStockDernierMaj: function() {
			return this.stock_dernier_maj;
		},
		getStockQuantite: function() {
			return this.stock_quantite;
		},
		getPrixId: function() {
			return this.prix_id;
		},
		getPrixValeur: function() {
			return this.prix_valeur;
		},
		getPrixDateDebut: function() {
			return this.prix_date_debut;
		},
		getPrixDateDebutFormated: function() {
			return this.prix_date_debut_formated;
		},
		getPrixDateFin: function() {
			return this.prix_date_fin;
		},
		getPrixDateFinFormated: function() {
			return this.prix_date_fin_formated;
		}, 
		getQuantiteVendue: function() {
			return this.quantite_vendue;
		},
		getDisponible: function() {
			return this.disponible;
		},

		//Setters
		setId: function(id) {
			this.id = id;
		},
		setNom: function(nom) {
			this.nom = nom;
		},
		setDescription: function(description) {
			this.description = description;
		},
		setCategorieId: function(categorie_id) {
			this.categorie_id = categorie_id;
		},
		setCategorieLibelle: function(categorie_libelle) {
			this.categorie_libelle = categorie_libelle;
		},
		setStockId: function(stock_id) {
			this.stock_id = stock_id;
		},
		setStockDernierMaj: function(stock_dernier_maj) {
			this.stock_dernier_maj = stock_dernier_maj;
		},
		setStockQuantite: function(stock_quantite) {
			this.stock_quantite = stock_quantite;
		},
		setPrixId: function(prix_id) {
			this.prix_id = prix_id;
		},
		setPrixValeur: function(prix_valeur) {
			this.prix_valeur = prix_valeur;
		},
		setPrixDateDebut: function(prix_date_debut) {
			this.prix_date_debut = prix_date_debut;
		},
		setPrixDateFin: function(prix_date_fin) {
			this.prix_date_fin = prix_date_fin;
		},
		setQuantiteVendue: function(quantite_vendue) {
			this.quantite_vendue = quantite_vendue;
		},
		setDisponible: function(dispo) {
			this.disponible = dispo;
		},

		setIntegerValues: function() {
			if(this.quantite_vendue) this.quantite_vendue = parseInt(this.quantite_vendue);
			if(this.stock_quantite) this.stock_quantite = parseInt(this.stock_quantite);

		},


		isCompleted: function() {
			var bool = (this.getNom() !== null && this.getDescription() !== null && this.getCategorieId() !== null && this.getCategorieId() !== "-1");

			bool = bool && (this.getStockQuantite() !== null && Number.isInteger(this.getStockQuantite()));

			bool = bool && (this.getPrixValeur() !== null && this.getPrixValeur().search(/[A-Z;a-z;]/g) < 0);


			bool = bool && (this.getPrixDateDebutFormated() !== null && this.getPrixDateFinFormated() !== null);


			return bool;
		},

		formateToForm: function() {
			//this.setPrixValeur(parseInt(this.getPrixValeur()));
			this.setStockQuantite(parseInt(this.getStockQuantite()));

			var debut = new Date(this.getPrixDateDebutFormated());
			var fin = new Date(this.getPrixDateFinFormated());

			this.setPrixDateDebut(debut);
			this.setPrixDateFin(fin);

			// this.setPrixDateDebut(("0" + debut.getDate()).slice(-2) + "/" + ("0" + (debut.getMonth() + 1)).slice(-2) + "/" + debut.getFullYear());
			// this.setPrixDateFin(("0" + fin.getDate()).slice(-2) + "/" + ("0" + (fin.getMonth() + 1)).slice(-2) + "/" + fin.getFullYear());

			return this;
		}
	};
	return Produit;
}]);