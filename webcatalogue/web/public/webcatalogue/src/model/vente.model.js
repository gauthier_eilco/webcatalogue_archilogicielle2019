angular.module('webcatalogue').factory('Vente', [function() {

	function Vente(data) {
		if(data) {
			this.setData(data);
		}
	}

	Vente.prototype = {

		//Attributs d'un produit
		id: null,
		date: null,
		prix_total: null,
		panier:null, //liste des produits avec la quantité vendue
		valider: null,
		total_produits: null,


		//Hydratation de l'objet
		setData: function(data) {
			angular.extend(this, data);
		},

		//Getters
		getId: function() {
			return this.id;
		},
		getDate: function() {
			return this.date;
		},
		getPrixTotal: function() {
			return this.prix_total;
		},
		getPanier: function() {
			return this.panier;
		},

		getValider: function() {
			return this.valider;
		},

		getTotalArticles: function() {
			return this.total_produits;
		},

		//Setters
		setId: function(id) {
			this.id = id;
		},
		setDate: function(date) {
			this.date = date;
		},
		setPrixTotal: function(prix_total) {
			this.prix_total = prix_total;
		},
		setPanier: function(panier) {
			this.panier = panier;
		},
		setValider: function(valider) {
			this.valider = valider;
		}

	};
	return Vente;
}]);