'use strict';
angular.module('webcatalogue').component('printerModal', {
    template:  
    [
      '<div id="{{$ctrl.modalObject.id}}" class="modal">',
        '<div class="modal-dialog">',
         '<div class="modal-content">',
            '<div class="modal-header">',
              '<h5 class="modal-title">{{$ctrl.modalObject.title}}</h5>',
              '<button type="button" class="close" ng-click="$ctrl.hide()" data-dismiss="modal" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<div class="alert" ng-class="{\'alert-success\' : $ctrl.modalObject.success, \'alert-warning\' : $ctrl.modalObject.warning}" role="alert">',
                    '{{ $ctrl.modalObject.message }}',
                '</div>',
            '</div>',
            '<div class="modal-footer">',
              '<button type="button" ng-click="$ctrl.hide()" class="btn btn-primary">OK</button>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      modalObject: '<'
    },
    controller: ['$timeout', '$document',  function($timeout, $document) {
       var ctrl = this;
       this.modal = null;
      
      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
      };

      this.show = function() {
        if(ctrl.modal === null) {
          var id = ctrl.modalObject.id;
          ctrl.modal = $document[0].getElementById(id);
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 
      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

    }]
  
});
angular.module('webcatalogue').component('customModalVenteReview', {
    template:  
    [
      '<div id="modalVenteReview" class="modal">',
         '<div class="modal-content avoid-full">',
            '<div class="modal-header">',
              '<h5 class="modal-title">Récapitulatif de la vente n° <b>{{$ctrl.vente.id}}</b></h5>',
              '<button type="button" class="close closeModal" ng-click="$ctrl.hide()" data-dismiss="modal" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<div id="cd-cart" style="width: 100% !important; position:relative !important; padding-top:0px !important;">',
                  '<h2>Produit(s) acheté(s)</h2>',
                    '<ul class="cd-cart-items">',
                      '<li ng-repeat="product in $ctrl.vente.panier">',
                      '<span class="cd-qty">{{product.quantite_vendue}}x</span> {{product.nom}} <span class="badge badge-warning" ng-show="product.disponible == 0">Indisponible</span>',
                      '<div class="cd-price">{{product.prix_valeur}} €</div>',
                      '</li>',
                    '</ul>',
                    '<div class="cd-cart-total">',
                      '<p>Total <span>{{$ctrl.vente.prix_total}} €</span></p>',
                    '</div>',
                '</div>',
            '</div>',
          '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      vente: '<'
    },
    controller: ['$timeout', '$document', function($timeout, $document) {
       var ctrl = this;
       this.modal = null;

      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
      };

     this.show = function() {
        if(ctrl.modal === null) {
          ctrl.modal = $document[0].getElementById("modalVenteReview");
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 
      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

    }]
  
});

angular.module('webcatalogue').component('customModalVente', {
    template:  
    [
      '<div id="modalVente" class="modal">',
        '<div class="modal-dialog">',
         '<div class="modal-content">',
            '<div class="modal-header">',
              '<h5 class="modal-title">Choisir la quantité pour ce produit</h5>',
              '<button type="button" class="close" ng-click="$ctrl.hide()" data-dismiss="modal" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<div class="col-xs-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text" >Quantité désirée</span>',
                    '</div>',
                        '<input type="number" ng-model="$ctrl.quantite"  class="form-control"  />',
                  '</div>',
                '</div>',
            '</div>',
            '<div class="modal-footer">',
              '<button type="button" ng-click="$ctrl.save()" ng-disabled="$ctrl.produit.stock_quantite < $ctrl.quantite || $ctrl.quantite <=0" class="btn btn-primary">Ajouter au panier</button>',
              '<button type="button" class="btn btn-warning" ng-click="$ctrl.hide()" data-dismiss="modal">Annuler</button>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      onSave: '&',
      produit: '<'
    },
    controller: ['$document', '$timeout', function($document, $timeout) {
       var ctrl = this;
       this.modal = null;
       this.formUpdateProduitComponent;
       this.quantite;
      
      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
        ctrl.quantite = parseInt("0");

      };


      this.save = function() {
        $timeout(function() {
          //mettre à jour la quantité vendue
          ctrl.produit.setQuantiteVendue(ctrl.produit.quantite_vendue + ctrl.quantite);

          //update le stock
          //var new_stock= ctrl.produit.stock_quantite - ctrl.quantite;
          //ctrl.produit.setStockQuantite(new_stock);
          
          ctrl.onSave({produit: ctrl.produit});
        });
        ctrl.hide();
       
      };

      this.show = function() {
        ctrl.quantite = 0;
        if(ctrl.modal === null) {

          ctrl.modal = $document[0].getElementById("modalVente");
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 

      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

    }]
  
});


angular.module('webcatalogue').component('customModal', {
    template:  
    [
      '<div id="modal" class="modal">',
        '<div class="modal-dialog modal-600">',
         '<div class="modal-content">',
            '<div class="modal-header">',
              '<h5 class="modal-title">{{$ctrl.title}}</h5>',
              '<button type="button" class="close" data-dismiss="modal" ng-click="$ctrl.hide()" aria-label="Close">',
                '<span aria-hidden="true">&times;</span>',
              '</button>',
            '</div>',
            '<div class="modal-body">',
                '<form-update-produit on-init="$ctrl.setFormUpdateProduit(form)" produit="$ctrl.produit"></form-update-produit>',
            '</div>',
            '<div class="modal-footer">',
              '<button type="button" ng-click="$ctrl.save()" ng-disabled="!$ctrl.produit.isCompleted()" class="btn btn-primary">Enregistrer les changements</button>',
              '<button type="button" class="btn btn-warning" ng-click="$ctrl.hide()" data-dismiss="modal">Annuler</button>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join(''),
    bindings: {
      onInit: '&',
      title: '@',
      onSave: '&',
      produit: '<'
    },
    controller: ['$document', '$timeout', 'Produit', 'APIService', 'AppTools', function($document, $timeout, Produit, APIService, AppTools) {
       var ctrl = this;
       this.modal = null;
       this.formUpdateProduitComponent;
      
      this.$onInit = function() {
        ctrl.onInit({modal: ctrl});
      };

      this.setFormUpdateProduit = function(form) {
        ctrl.formUpdateProduitComponent = form;
      };

      this.save = function() {
        ctrl.formatePrixDateDebut();
        ctrl.formatePrixDateFin();
        ctrl.onSave({produit: ctrl.produit});
      };

      this.show = function() {
        ctrl.quantite = 0;
        if(ctrl.modal === null) {
          ctrl.modal = $document[0].getElementById("modal");
        }
        
        ctrl.modal.style.display = "block";
        $timeout(function(){ ctrl.modal.style.opacity = 1; }); //FOR TRANSITION 

      };

      this.hide= function() {
        ctrl.modal.style.display = "none";
      };

      this.formatePrixDateDebut = function() {
          ctrl.produit.prix_date_debut_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_debut);
      };

       this.formatePrixDateFin = function() {
          ctrl.produit.prix_date_fin_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_fin);
      };

    }]
  
});


angular.module('webcatalogue').component('componentManager', {
    template:   
  [
    '<form-ajout-produit produit="$ctrl.produit" on-send="$ctrl.registerProduit(produit)" ng-show="$ctrl.action==\'ajout-produit\'" on-init="$ctrl.setFormAjoutProduit(form)"></form-ajout-produit>',
    '<list-produits panier="$ctrl.panier" ng-show="$ctrl.action==\'produits\'" on-init="$ctrl.setListeProduits(grid)"></list-produits>',
    '<list-ventes ng-show="$ctrl.action==\'ventes\'" on-init="$ctrl.setListeVente(grid)"></list-ventes>'
  ].join(''),
    bindings: {
        action: '<',
        onInit: '&',
        produit: '<',
        panier: '='
    },
    controller: ['$element', '$timeout', 'Produit', 'APIService',  function($element, $timeout, Produit, APIService) {
       var ctrl = this;
       this.produit;
       this.formAjoutProduitComponent;
       this.listeProduitComponent;
       this.listeVenteComponenent;

       this.setFormAjoutProduit = function(form) {
          ctrl.formAjoutProduitComponent = form;
       };

       this.setListeProduits = function(grid) {
          ctrl.listeProduitComponent = grid;
       };

       this.setListeVente = function(grid) {
          ctrl.listeVenteComponenent = grid;
       };


       this.$onInit = function() {
          ctrl.onInit({component: ctrl});
       };

       this.$onChanges = function() {
          // console.log(ctrl.produit);
          
          //Quand l'action change, je recharge le tableau des produits si l'utilisateur a cliqué sur le menu 'liste des produits'
          if(ctrl.action == "produits")
              $timeout(function() {
                ctrl.listeProduitComponent.loadProduits();
              });
          else if(ctrl.action == "ventes")
            $timeout(function() {
                ctrl.listeVenteComponenent.loadVentes();
            });
       };

       this.registerProduit = function() {
          APIService.sendProduitToBdd(ctrl.produit).then(function success() {
              //Modal
              $timeout(function() {
                  ctrl.formAjoutProduitComponent.printModal();
                  ctrl.formAjoutProduitComponent.resetForm();
              }, 300);
             
          });
       };

    }]
});


//Formulaire d'ajout d'un produit
angular.module('webcatalogue').component('formAjoutProduit', {
    template:   
    [
      '<printer-modal on-init="$ctrl.setPrinterModal(modal)" modal-object="$ctrl.printerModalObject"></printer-modal>',
      '<h3 class="text-center" style="margin-bottom: 20px">Formulaire d\'ajout d\'un nouveau produit</h3>',
       '<form id="formAjout">',
          '<div class="row">',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Nom</span>',
                    '</div>',
                        '<input type="text" ng-class="{\'is-invalid\' : $ctrl.produit.nom == \'\'}" ng-model="$ctrl.produit.nom" class="form-control">',
                  '</div>',
                '</div>',

                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Catégorie</span>',
                    '</div>',
                    '<select ng-model="$ctrl.produit.categorie_id" class="form-control form-control-sm">',
                        '<option ng-selected="selected" value="-1">Sélectionner une catégorie</option>',
                        '<option ng-repeat="c in $ctrl.categories" value="{{c.value}}">{{c.libelle}}</option>',
                    '</select>',
                  '</div>',
                '</div>',

                '<div class="col-md-4" style="padding:10px">',
                      '<div class="input-group input-group-sm">',
                        '<div class="input-group-prepend">',
                          '<span class="input-group-text">Stock</span>',
                        '</div>',
                            '<input type="number" ng-class="{\'is-invalid\' : $ctrl.produit.stock_quantite < 0}" ng-model="$ctrl.produit.stock_quantite" class="form-control">',
                      '</div>',
                    '</div>',
                '</div>',
            '</div>',

            '<div class="row">',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Prix</span>',
                    '</div>',
                        '<input type="text" ng-class="{\'is-invalid\' : $ctrl.produit.prix_valeur < 0 || $ctrl.produit.prix_valeur.search($ctrl.regexNumber) > -1}" ng-model="$ctrl.produit.prix_valeur" class="form-control">',
                  '</div>',
                '</div>',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date début</span>',
                    '</div>',
                        '<input type="date" ng-change="$ctrl.formatePrixDateDebut()" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_debut}" ng-model="$ctrl.produit.prix_date_debut" class="form-control">',
                  '</div>',
                '</div>',
                '<div class="col-md-4" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date fin</span>',
                    '</div>',
                        '<input type="date" ng-change="$ctrl.formatePrixDateFin()" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_fin}" ng-model="$ctrl.produit.prix_date_fin" class="form-control">',
                  '</div>',
                '</div>',
            '</div>',

             '<div class="row">',
                '<div class="col-md-12" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend ">',
                      '<span class="input-group-text">Description</span>',
                    '</div>',
                    '<textarea ng-model="$ctrl.produit.description" rows="5" cols="55">',
                    '</textarea>',
                  '</div>',
                '</div>',
            '</div>',

            '<div class="row">',
              '<div class="col-md"12" style="text-align:center">',
                  '<button type="button" ng-disabled="!$ctrl.produit.isCompleted()" class="btn btn-success" ng-click="$ctrl.sendProduit()">Envoyer</button>',
              '</div>',
            '</div>',
        '</form>'
    ].join(''),
    bindings: {
        onInit: '&',
        produit: '<',
        onSend: '&'
    },
    controller: ['$element', '$timeout', 'Produit', 'AppTools', 'APIService', 'ValuesProvider', function($element, $timeout, Produit, AppTools, APIService, ValuesProvider) {
       var ctrl = this;
       this.categories = [];
       this.regexNumber;
       this.printerModal;
       this.printerModalObject = {};

       this.$onInit = function() {
          ctrl.onInit({form: ctrl});
          APIService.getCategories().then(function(response) {
              ctrl.categories = response.data;
          });
          ctrl.regexNumber = /[A-Z;a-z;]/g;

          $timeout(function() {
            $element.find("input.form-control").css("width", 330);
          });
       };

       this.setPrinterModal = function(modal) {
          this.printerModal = modal;
       };
       
       this.formatePrixDateDebut = function() {
          ctrl.produit.prix_date_debut_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_debut);
       };

       this.formatePrixDateFin = function() {
          ctrl.produit.prix_date_fin_formated =  AppTools.convertStrToDate( ctrl.produit.prix_date_fin);
       };

       this.sendProduit = function() {
          ctrl.onSend({produit: ctrl.produit});           
       };

       this.resetForm = function() {
          ctrl.produit = new Produit();
       };

       this.printModal = function() {
          ctrl.printerModalObject = ValuesProvider.getModalObjectSuccessAjout();
          $timeout(function() {
              ctrl.printerModal.show();
          });
       };

       

    }]
});

//Formulaire de modification d'un produit -> appelé dans le modal "customModal"
angular.module('webcatalogue').component('formUpdateProduit', {
    template:   
    [
       '<form id="formUpdate">',
          '<div class="row">',
              '<div class="col-md-12" style="padding:10px">',
                '<div class="input-group input-group-sm">',
                  '<div class="input-group-prepend">',
                    '<span class="input-group-text">Identifiant</span>',
                  '</div>',
                      '<input type="text" ng-model="$ctrl.produit.id" class="form-control" disabled>',
                '</div>',
              '</div>',
            '</div>',
             '<div class="row">',
                  '<div class="col-md-12" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Nom</span>',
                      '</div>',
                          '<input type="text" ng-model="$ctrl.produit.nom" class="form-control">',
                    '</div>',
                  '</div>',
               '</div>',
               '<div class="row">',
                  '<div class="col-md-12" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Catégorie</span>',
                      '</div>',
                      '<select ng-model="$ctrl.produit.categorie_id" class="form-control form-control-sm">',
                          '<option ng-selected="selected" value="-1">Sélectionner une catégorie</option>',
                          '<option ng-repeat="c in $ctrl.categories" value="{{c.value}}">{{c.libelle}}</option>',
                      '</select>',
                    '</div>',
                  '</div>',
                '</div>',
                '<div class="row">',
                  '<div class="col-md-6" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Stock</span>',
                      '</div>',
                          '<input type="number" ng-class="{\'is-invalid\' : $ctrl.produit.stock_quantite < 0}" ng-model="$ctrl.produit.stock_quantite" class="form-control">',
                    '</div>',
                  '</div>',
                  '<div class="col-md-6" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend">',
                        '<span class="input-group-text">Prix</span>',
                      '</div>',
                          '<input type="text" ng-class="{\'is-invalid\' : $ctrl.produit.prix_valeur < 0 || $ctrl.produit.prix_valeur.search($ctrl.regexNumber) > -1}" ng-model="$ctrl.produit.prix_valeur" class="form-control">',
                    '</div>',
                  '</div>',
                '</div>',
                '<div class="row">',
                  '<div class="col-md-12" style="padding:10px">',
                    '<div class="input-group input-group-sm">',
                      '<div class="input-group-prepend ">',
                        '<span class="input-group-text">Description</span>',
                      '</div>',
                      '<textarea ng-model="$ctrl.produit.description" rows="5" cols="52">',
                      '</textarea>',
                    '</div>',
                  '</div>',
              '</div>',
              '<div class="row">',
                '<div class="col-md-6" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date début</span>',
                    '</div>',
                        '<input type="date" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_debut}" ng-model="$ctrl.produit.prix_date_debut" class="form-control">',
                  '</div>',
                '</div>',
                '<div class="col-md-6" style="padding:10px">',
                  '<div class="input-group input-group-sm">',
                    '<div class="input-group-prepend">',
                      '<span class="input-group-text">Date fin</span>',
                    '</div>',
                        '<input type="date" ng-class="{\'is-invalid\' : !$ctrl.produit.prix_date_fin}" ng-model="$ctrl.produit.prix_date_fin" class="form-control">',
                  '</div>',
                '</div>',
            '</div>',
        '</form>'
    ].join(''),
    bindings: {
        onInit: '&',
        produit: '<',
        onSend: '&'
    },
    controller: ['$element', '$timeout', 'Produit', 'AppTools', 'APIService', function($element, $timeout, Produit, AppTools, APIService) {
       var ctrl = this;
       this.categories = [];
       this.regexNumber;

       this.$onInit = function() {
          ctrl.onInit({form: ctrl});
          APIService.getCategories().then(function(response) {
              ctrl.categories = response.data;
          });
          ctrl.regexNumber = /[A-Z;a-z;]/g;
         
       };
       

    }]
});

angular.module('webcatalogue').component('listProduits', {
    template:   
  [
   '<printer-modal on-init="$ctrl.setPrinterModal(modal)" modal-object="$ctrl.printerModalObject"></printer-modal>',
    '<custom-modal-vente on-init="$ctrl.setModalVenteComponent(modal)" produit="$ctrl.selectedProduit" on-save="$ctrl.alimenterPanier(produit)"></custom-modal-vente>',
    '<custom-modal produit="$ctrl.selectedProduit" title="Modification du produit" on-init="$ctrl.setModalComponent(modal)" on-save="$ctrl.updateProduit(produit)"></custom-modal>',
    '<table class="table col-md-12">',
        '<thead class="thead-light">',
            '<tr>',
                '<th scope="col"></th>',
                '<th scope="col">Id</th>',
                '<th scope="col">Nom</th>',
                '<th scope="col">Description</th>',
                '<th scope="col">Catégorie</th>',
                '<th scope="col">Mise à jour du stock</th>',
                '<th scope="col">Quantité en stock</th>',
                '<th scope="col">Prix</th>',
                '<th scope="col">Date début</th>',
                '<th scope="col">Date fin</th>',
            '</tr>',
        '</thead>',
        '<tbody>',
            '<tr ng-repeat="produit in $ctrl.produits" ng-class="{\'table-warning\' : produit.stock_quantite == 0}">',
                '<th scope="col">',
                  '<div class="btn-group">',
                    '<button type="button" style="margin-right:5px" ng-click="$ctrl.showModalEditing(produit)" class="btn btn-sm btn-warning">Modifier</button>',
                    '<button type="button" style="margin-right:5px" ng-click="$ctrl.addToKart(produit)" class="btn btn-sm btn-success">Ajouter au panier</button>',
                    '<button type="button" style="margin-right:5px" ng-click="$ctrl.removeProduit(produit)" class="btn btn-sm btn-danger">Supprimer</button>',
                  '</div>',
                '</th>',
                '<th scope="col">{{produit.id}}</th>',
                '<th scope="col">{{produit.nom}}</th>',
                '<th scope="col">{{produit.description}}</th>',
                '<th scope="col">{{produit.categorie_libelle}}</th>',
                '<th scope="col">{{$ctrl.formateDate(produit.stock_dernier_maj)}}</th>',
                '<th scope="col">{{produit.stock_quantite}} en stock</th>',
                '<th scope="col">{{produit.prix_valeur}} €</th>',
                '<th scope="col">{{$ctrl.formateDate(produit.prix_date_debut_formated)}}</th>',
                '<th scope="col">{{$ctrl.formateDate(produit.prix_date_fin_formated)}}</th>',
            '</tr>',
        '</tbody>',
    '</table>'
  ].join(''),
    bindings: {
        onInit: '&',
        panier: '='
    },
    controller: ['$element', '$timeout', 'Produit', 'AppTools', 'APIService', 'ValuesProvider',  function($element, $timeout, Produit, AppTools, APIService, ValuesProvider) {
      var ctrl = this;
      this.produits = [];
      this.modalComponent;
      this.modalVenteComponent;
      this.selectedProduit = {};
      this.printerModal;
      this.printerModalObject = {};

       this.$onInit = function() {
          ctrl.onInit({grid: ctrl});
       };

       this.setPrinterModal = function(modal) {
          ctrl.printerModal = modal;
       };

       this.updateProduit = function(produit) {

          //APIService
          APIService.updateProduit(produit).then(function success() {
            
            //Reload le tableau des produits
            $timeout(function() {
              ctrl.modalComponent.hide();
              ctrl.loadProduits();
            });

          });
          
       };

       this.setModalComponent = function(modal) {
          ctrl.modalComponent = modal;
       };

       this.setModalVenteComponent = function(modal) {
        ctrl.modalVenteComponent = modal;
       };

       this.loadProduits = function() {
          ctrl.produits = [];
          APIService.getProduits().then(function(response) {
              $timeout(function() {
                  response.data.forEach(function(object){
                     ctrl.produits.push(new Produit(object));
                  });
              });
          });
       };

       this.formateDate = function(date) {
          return AppTools.formateDateForGrid(date);
       };

       this.showModalEditing = function(produit) {

          ctrl.selectedProduit = new Produit(produit.formateToForm());
          $timeout(function() {
              ctrl.modalComponent.show();
          });
       };

       this.showModalVente = function() {
          $timeout(function() {
              ctrl.modalVenteComponent.show();
          });
       };

       this.addToKart = function(produit) {
          ctrl.selectedProduit = new Produit(produit);
          ctrl.showModalVente();
       };

       this.removeProduit = function(produit) {
            var found = {};
            found = AppTools.findProduitInPanier(ctrl.panier, produit.id);

            if(found.find) { //Message de warning : le produit est actuellement dans le panier
                ctrl.printerModalObject = ValuesProvider.getModalObjectWarningPanier();

                $timeout(function() {
                    ctrl.printerModal.show();
                });
            }
            else { //Suppression dans la bdd selon s'il appartient à une vente 
              APIService.deleteProduit(produit);
                ctrl.printerModalObject = ValuesProvider.getModalObjectSuccessSuppressionProduit();
                $timeout(function() {
                  ctrl.printerModal.show();
                  ctrl.loadProduits();
                });
            }

            
       };

       this.alimenterPanier = function(produit) {
          var found = {};
          found = AppTools.findProduitInPanier(ctrl.panier, produit.id);
          //console.log(found);         
          if(!found.find) {
              ctrl.panier.produits.push(produit);
              ctrl.panier.vente.setPrixTotal(ctrl.panier.vente.prix_total + (parseFloat(produit.prix_valeur) * parseInt(produit.quantite_vendue)));
          }
          else {
              found.produit.setQuantiteVendue(found.produit.quantite_vendue + produit.quantite_vendue);
              ctrl.panier.vente.setPrixTotal(ctrl.panier.vente.prix_total + (parseFloat(produit.prix_valeur) * parseInt(produit.quantite_vendue)));
          }
       };

    }]
});

angular.module('webcatalogue').component('listVentes', {
    template:   
    [
        '<custom-modal-vente-review on-init="$ctrl.setModalVenteReview(modal)" vente="$ctrl.selectedVente"></custom-modal-vente-review>',
        '<table class="table col-md-12">',
            '<thead class="thead-light">',
                '<tr>',
                    '<th scope="col"></th>',
                    '<th scope="col">Id</th>',
                    '<th scope="col">Date vente</th>',
                    '<th scope="col">Prix total</th>',
                    '<th scope="col">Nombre d\'articles vendues</th>',
                '</tr>',
            '</thead>',
            '<tbody>',
                '<tr ng-repeat="vente in $ctrl.ventes">',
                    '<th scope="col">',
                      '<div class="btn-group">',
                        '<button type="button" class="btn btn-sm btn-info" ng-click="$ctrl.loadVente(vente)">',
                          'Voir le détail',
                        '</button>',
                      '</div>',
                    '</th>',
                    '<th scope="col">{{vente.id}}</th>',
                    '<th scope="col">{{$ctrl.formateDate(vente.date)}}</th>',
                    '<th scope="col">{{vente.prix_total}}</th>',
                    '<th scope="col">{{vente.total_produits}}</th>',
                '</tr>',
            '</tbody>',
        '</table>'
    ].join(''),
    bindings: {
      onInit: '&'
    },
    controller: ['$element', '$timeout', 'Produit','Vente', 'AppTools', 'APIService',  function($element, $timeout, Produit, Vente, AppTools, APIService) {
       var ctrl = this;
       this.ventes = [];
       this.selectedVente;
       this.modalVenteReview;

       this.$onInit = function() {
          ctrl.onInit({grid: ctrl});
       };

       this.setModalVenteReview = function(modal) {
        this.modalVenteReview = modal;
       };

       this.loadVentes = function() {
          ctrl.ventes = [];
          APIService.getVentes().then(function(response) {
              $timeout(function() {
                  response.data.forEach(function(object){
                     ctrl.ventes.push(new Vente(object));
                  });
              });
          });
       };

       this.loadVente = function(vente) {
          APIService.getVente(vente.id).then(function(response) {
              ctrl.selectedVente = new Vente(vente);
              ctrl.selectedVente.panier = [];
              response.data.forEach(function(produit) {
                ctrl.selectedVente.panier.push(new Produit(produit));
              });
              //console.log(ctrl.selectedVente);
              ctrl.modalVenteReview.show();
          });
       };

        this.formateDate = function(date) {
          return AppTools.formateDateForGrid(date);
       };


    }]
});
