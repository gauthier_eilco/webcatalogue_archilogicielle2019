var eslint = require("gulp-eslint");
var src = './src/';

gulp.task('eslint', function () {
    return gulp.src(src + '**/*.js')
        .pipe(eslint())
        .pipe(eslint.format('stylish'));
});