-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 02 fév. 2020 à 10:42
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

DROP DATABASE IF EXISTS webcatalogue;
CREATE DATABASE webcatalogue;
USE webcatalogue;

--
-- Base de données :  `webcatalogue`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `libelle`) VALUES
(1, 'DVD'),
(2, 'Cosmétique'),
(3, 'Instrument ménager'),
(4, 'Livre'),
(5, 'Electroménager'),
(6, 'Informatique');

-- --------------------------------------------------------

--
-- Structure de la table `prix`
--

DROP TABLE IF EXISTS `prix`;
CREATE TABLE IF NOT EXISTS `prix` (
  `id_prix` int(11) NOT NULL AUTO_INCREMENT,
  `valeur` float NOT NULL,
  `date_debut` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_fin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_prix`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `description` varchar(250) NOT NULL,
  `disponible` int(1) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `prix_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_produit_categorie` (`categorie_id`),
  KEY `fk_produit_stock` (`stock_id`),
  KEY `fk_produit_prix` (`prix_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `produit_vente`
--

DROP TABLE IF EXISTS `produit_vente`;
CREATE TABLE IF NOT EXISTS `produit_vente` (
  `id_produit` int(11) NOT NULL,
  `id_vente` int(11) NOT NULL,
  `quantite_vendue` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_produit`,`id_vente`),
  KEY `fk_produit_vente_vente` (`id_vente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stock_produit`
--

DROP TABLE IF EXISTS `stock_produit`;
CREATE TABLE IF NOT EXISTS `stock_produit` (
  `id_stock` int(11) NOT NULL AUTO_INCREMENT,
  `dernier_maj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Quantite` int(8) NOT NULL,
  PRIMARY KEY (`id_stock`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `vente`
--

DROP TABLE IF EXISTS `vente`;
CREATE TABLE IF NOT EXISTS `vente` (
  `id_vente` int(11) NOT NULL AUTO_INCREMENT,
  `date_vente` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prix_totale` float NOT NULL,
  `valider` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_vente`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `fk_produit_categorie` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `fk_produit_prix` FOREIGN KEY (`prix_id`) REFERENCES `prix` (`id_prix`),
  ADD CONSTRAINT `fk_produit_stock` FOREIGN KEY (`stock_id`) REFERENCES `stock_produit` (`id_stock`);

--
-- Contraintes pour la table `produit_vente`
--
ALTER TABLE `produit_vente`
  ADD CONSTRAINT `fk_produit_vente_produit` FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id`),
  ADD CONSTRAINT `fk_produit_vente_vente` FOREIGN KEY (`id_vente`) REFERENCES `vente` (`id_vente`);
COMMIT;

