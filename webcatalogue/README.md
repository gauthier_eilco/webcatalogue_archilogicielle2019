wiki
====

A Symfony project created on September 5, 2019, 6:26 pm.

Projet Symfony
===

Le projet Symfony se trouve dans **webcatalogue\src\AppBundle** et dans **webcatalogue\app\Resources\views** pour la vue principale index.html.twig qui appelle le projet angularJS.


Instructions : 
===

Pour pouvoir lancer le serveur symfony, il faudra nécessiter d'une version de php (7.*) sur le poste (de préférence directement intallée sur le poste et en variable d'environnement) et d'un serveur de base de données (mysql de préférence avec wamp server par exemple).

Pour installer les dépendances de symfony, lancer la commande **php composer.phar install** en se plaçant dans le répertoire **webcatalogue**. Il installera alors toutes les dépendances dans un dossier "vendor".

Ensuite, renseignez les paramètres d'accès à la base de données dans le fichier : **webcatalogue\app\config\parameters.yml**.

Le fichier source de la base de données est dans le repertoire **webcatalogue\sql**, la base de données s'appelle "webcatalogue".

Lancez la totalité du projet via la commande : **php bin\console server:run** en se plaçant dans le répertoire **webcatalogue**.

Un problème que vous pourrez rencontrer stipulera qu'une classe n'est pas trouvée : il s'agit normalement de la classe UnitOfWork. Il suffit de remplacer les instructions "continue" (comme expliqué sur le message d'erreur) par "continue 2". Le fichier php de la classe se trouve dans : **vendor\doctrine\orm\lib\Doctrine\ORM\UnitOfWork.php**.


Projet AngularJS
===

Le projet AngularJS se trouve dans **webcatalogue\web\public\webcatalogue**.

Les tests unitaires, réalisés avec karma et jasmine, sont dans **webcatalogue\web\public\webcatalogue\test**. Ils concernent principalement la partie model de l'application à savoir les classes Produit et Vente.


Instructions 
===

Pour réinstaller les dépendances du projet (dossier node_modules), se déplacer dans **webcatalogue\web** et lancer la commande **npm install** (Il réinstallera alors les dépendances listées dans le fichier **package.json** situé dans ce même répertoire).

Pour effectuer les tests unitaires, il faut installer (angular et angular-mocks comme bower components) via la commande : **bower install angular** et **bower install angular-mocks** en se plaçant toujours dans le répertoire **webcatalogue\web**.


Tests unitaires et EsLinter
===

Pour lancer les tests, la commande est : **npm run unit-test**.

Pour lancer le linter, la commande est : **npm run eslinter**. Il est géré par l'automatiseur de tâches "Gulp".

